from armarx.interface_helper import load_armarx_slice

load_armarx_slice('ArmarXCore', 'core/SharedMemory.ice')
# load_armarx_slice('ArmarXCore', 'core/BasicVectorTypes.ice')
from armarx import SharedMemoryProviderInterface, MetaInfoSizeBase


class SharedMemoryProvider(SharedMemoryProviderInterface):
    def __init__(self, size):
        super(SharedMemoryProviderInterface, self).__init__()
        self.size = size
        self.timestamp = 0
        self.metaInfo = MetaInfoSizeBase(self.size, self.size, self.timestamp)
        self.data = []
    def getMetaInfo(self, current = None):
        # print "Returning meta info: "+ str(self.metaInfo.size)
        self.metaInfo.timeProvided = self.timestamp
        return self.metaInfo

    def getData(self, current = None):
        # print "Returning data"
        return (self.data, self.metaInfo)

    def getSize(self, current = None):
        return self.size

    def getHardwareId(self, current = None):
        return "00:00:00:00:00:00"