#!/usr/bin/env python3
import os
import time
import logging
import numpy as np
import skimage.transform
import skimage.io

from armarx.parser import ArmarXArgumentParser as ArgumentParser

from armarx.ice_manager import wait_for_proxy
from armarx.ice_manager import is_alive
from armarx.ice_manager import get_proxy

from armarx.image_processor import ImageProcessor
from visionx import ImageProviderInterfacePrx

from mrcnn import model as modellib
from mrcnn import visualize
from mrcnn.config import Config

logger = logging.getLogger(__name__)


class MaskRCNNImageProcessorConfig(Config):
    """Configuration for training on the toy dataset.
    Derives from the base mrcnn.Config class and overrides some values.
    """
    NAME = 'armarx'
    IMAGES_PER_GPU = 1
    DETECTION_MIN_CONFIDENCE = 0.90
    GPU_COUNT = 1
    NUM_CLASSES = 1 + 80
    # Additional parameters specific to instances of MaskRCNNImageProcessorConfig
    MODEL_DIR = ''
    WEIGHTS_PATH = ''
    PRINT_RESULTS = True
    IGNORED_CLASSES = []


class MaskRCNNImageProcessor(ImageProcessor):
    class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
                   'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant',
                   'stop sign', 'parking meter', 'bench', 'bird',
                   'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
                   'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
                   'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
                   'kite', 'baseball bat', 'baseball glove', 'skateboard',
                   'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
                   'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
                   'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog',
                   'pizza', 'donut', 'cake', 'chair', 'couch', 'potted plant',
                   'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse',
                   'remote', 'keyboard', 'cell phone', 'microwave', 'oven',
                   'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase',
                   'scissors', 'teddy bear', 'hair drier', 'toothbrush']

    def __init__(self, maskrcnn_config, provider_name, num_result_images, image_number=0):
        super().__init__(provider_name, num_result_images)
        self.image_number = image_number

        # init mask rcnn
        self.config = maskrcnn_config

        class_name_to_id = dict(zip(self.class_names, range(len(self.class_names))))
        self.ignored_class_ids = [class_name_to_id[i] for i in self.config.IGNORED_CLASSES if i in self.class_names]

        self.colors = visualize.random_colors(self.config.NUM_CLASSES)
        self.model = modellib.MaskRCNN(mode='inference', config=self.config, model_dir=self.config.MODEL_DIR)
        self.model.load_weights(self.config.WEIGHTS_PATH, by_name=True)
        self.model.keras_model._make_predict_function()

    def process_image(self, images):
        # return a copy of all the input images plus the resulting segmentation
        result_images_shape = list(images.shape)
        result_images_shape[0] = images.shape[0] + 1
        result_images = np.zeros(result_images_shape, dtype=np.uint8)
        result_images[:-1] = images.copy()

        image = result_images[self.image_number]
        r = self.model.detect([image], verbose=0)[0]
        r = self.remove_ignored_classes(r)
        logger.debug('done detecting')

        if self.config.PRINT_RESULTS:
            r_class_names = [self.class_names[i] for i in r['class_ids']]
            print(list(zip(r_class_names, r['scores'])))
            print('detected classes: ' + ', '.join(r_class_names))

        # debug view
        # visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'], self.class_names, r['scores'])

        result_images[-1] = self.gen_segmentation(images[0], r['rois'], r['masks'], r['class_ids'], False)
        # result_images[-1] = self.color_splash(image, r['masks'])
        return result_images

    def gen_segmentation(self, image, boxes, masks, class_ids, map_color=False):
        """
            Segment the image according to the result of MaskRCNN.
            Note that any overlap conflicts are resolved in favor of the instance with the smaller bbox area.
            boxes: [num_instance, (y1, x1, y2, x2, class_id)] in image coordinates.
            masks: [height, width, num_instances]
            class_ids: [num_instances]

            Returns a label/color map from the given masks.
        """
        img = np.zeros(image.shape, dtype=np.uint8)
        # check number of instances
        if not ((boxes.shape[0] == masks.shape[-1]) and (boxes.shape[0] == class_ids.shape[0])):
            return img

        # deal with overlapping masks by preferring the instance with smaller bbox area
        # sort instances w.r.t. their bounding box area in descending order
        # boxes[i, :] = [y1, x1, y2, x2]
        areas = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        areas = areas[::-1].argsort()
        # the iteration order allows us to resolve overlaps by simply repainting the affected regions
        for i in areas:
            mask = masks[:, :, i]
            if map_color:
                color = self.colors[class_ids[i]]
                for c in range(3):
                    img[:, :, c] = np.where(mask == 1, color[c] * 255, img[:, :, c])
            else:
                img[:, :, 0] = np.where(mask == 1, class_ids[i], img[:, :, 0])

        return img

    def remove_ignored_classes(self, r):
        to_delete = [i for i, class_id in enumerate(r['class_ids']) if class_id in self.ignored_class_ids]
        r['rois'] = np.delete(r['rois'], to_delete, 0)
        r['masks'] = np.delete(r['masks'], to_delete, 2)
        r['class_ids'] = np.delete(r['class_ids'], to_delete)
        return r

    def color_splash(self, image, mask):
        """Apply color splash effect.
        image: RGB image [height, width, 3]
        mask: instance segmentation mask [height, width, instance count]

        Returns result image.
        """
        # Make a grayscale copy of the image.
        # The grayscale copy still has 3 RGB channels, though.
        # gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
        gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 0

        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        # Copy color pixels from the original color image where mask is set
        if mask.shape[0] > 0:
            splash = np.where(mask, image, gray).astype(np.uint8)
        else:
            # splash = gray
            splash = np.uint8(gray)
        return splash


def main():
    parser = ArgumentParser(description='MaskRCNN for ArmarX')
    parser.add_argument('-n', '--component-name', default='MaskRCNN', help='component name of the image provider')
    parser.add_argument('-i', '--input-name', default='KLGImageProvider', help='name of the image provider')
    parser.add_argument('-d', '--data-dir', default='/common/homes/staff/grotz/libs/mask-fusion/deps/Mask_RCNN/data')
    parser.add_argument('-c', '--confidence', default=0.9, type=float, help='The minimum confidence value')
    parser.add_argument('--ignore', default='', type=str, help='Comma separated list of classes to ignore; e.g. '
                                                               '--ignore \"tv, dining table, refrigerator, remote\"')
    args = parser.parse_args()

    image_number = 0
    provider_name = args.input_name.split(':')[0]
    if ':' in provider_name:
        provider_name, image_number = provider_name.split(':')
        image_number = int(image_number)

    logger.debug('trying to connect to image provider {}:{}'.format(provider_name, image_number))
    wait_for_proxy(ImageProviderInterfacePrx, provider_name)

    image_source = get_proxy(ImageProviderInterfacePrx, provider_name)
    num_result_images = image_source.getNumberImages() + 1

    maskrcnn_config = MaskRCNNImageProcessorConfig()
    maskrcnn_config.DETECTION_MIN_CONFIDENCE = args.confidence
    maskrcnn_config.MODEL_DIR = os.path.join(args.data_dir, 'logs')
    maskrcnn_config.WEIGHTS_PATH = os.path.join(args.data_dir, 'mask_rcnn_coco.h5')
    # maskrcnn_config.PRINT_RESULTS = False
    maskrcnn_config.IGNORED_CLASSES = [name.strip() for name in args.ignore.split(',') if name.strip()]
    if maskrcnn_config.IGNORED_CLASSES:
        print("Ignoring classes: " + str(maskrcnn_config.IGNORED_CLASSES))

    image_processor = MaskRCNNImageProcessor(maskrcnn_config, args.input_name, num_result_images, image_number)
    image_processor.register()

    try:
        while is_alive():
            time.sleep(0.1)
    except KeyboardInterrupt:
        logger.info('shutting down')
    finally:
        image_processor.shutdown()


if __name__ == '__main__':
    main()
