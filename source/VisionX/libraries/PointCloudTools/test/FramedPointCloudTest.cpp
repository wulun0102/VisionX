/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudTools
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::PointCloudTools

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../FramedPointCloud.h"

#include <iostream>
#include <fstream>
#include <filesystem>

#include <pcl/common/io.h>

#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/math/Helpers.h>


using namespace visionx;
namespace fs = std::filesystem;

using PointT = pcl::PointXYZ;


bool isClose(float lhs, float rhs, float prec = 1e-6f)
{
    return std::abs(lhs - rhs) < prec;
}

namespace pcl
{
    bool operator!=(const PointT& lhs, const PointT& rhs)
    {
        return !isClose(lhs.x, rhs.x) && !isClose(lhs.y, rhs.y) && !isClose(lhs.z, rhs.z);
    }
    std::ostream& operator<<(std::ostream& os, const PointT& rhs)
    {
        return os << "[" << rhs.x << " " << rhs.y << " " << rhs.z << "]";
    }
}

static const fs::path ROBOT_PATH = "TestBot.xml";
static const std::string ROBOT_XML =
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
    "<Robot Type=\"TestBot\" RootNode=\"Root\">"
    "   <RobotNode name=\"Root\">"
    "       <Child name=\"Node\"/>"
    "   </RobotNode>"
    "   <RobotNode name=\"Node\">"
    "       <Transform>"
    "           <Translation x=\"100\" y=\"200\" z=\"300\"/>"
    "           <!--rollpitchyaw roll=\"0\" pitch=\"-45\" yaw=\"0\" units=\"degree\"/-->"
    "       </Transform>"
    "   </RobotNode>"
    "</Robot>";


struct RobotFixture
{
    VirtualRobot::RobotPtr robot;

    RobotFixture()
    {
        BOOST_REQUIRE(!fs::exists(ROBOT_PATH));
        {
            std::ofstream ofs(ROBOT_PATH);
            ofs << ROBOT_XML;
        }
        BOOST_REQUIRE(fs::is_regular_file(ROBOT_PATH));
        robot = VirtualRobot::RobotIO::loadRobot(ROBOT_PATH, VirtualRobot::RobotIO::RobotDescription::eStructure);
        BOOST_REQUIRE(robot);
    }

    ~RobotFixture()
    {
        fs::remove(ROBOT_PATH);
        BOOST_REQUIRE(!fs::exists(ROBOT_PATH));
    }
};

struct PointCloudFixture
{
    pcl::PointCloud<PointT> original;
    FramedPointCloud<PointT> pointCloud;

    PointCloudFixture()
    {
        pointCloud = FramedPointCloud<PointT>("Node");
        pointCloud.cloud->push_back({ 0, 0, 0 });
        pointCloud.cloud->push_back({ 1, 0, 0 });
        pointCloud.cloud->push_back({ 0, 1, 1 });
        pointCloud.cloud->push_back({ 0, 0, 1 });

        pcl::copyPointCloud(*pointCloud.cloud, original);
    }

    pcl::PointCloud<PointT> getTransformedOriginal(const Eigen::Matrix4f& tf) const
    {
        pcl::PointCloud<PointT> result;
        pcl::transformPointCloud(original, result, tf);
        return result;
    }
};

struct Fixture : public RobotFixture, public PointCloudFixture
{
};


BOOST_FIXTURE_TEST_SUITE(FramedPointCloudTest, Fixture)


BOOST_AUTO_TEST_CASE(test_changeFrame_from_local_to_root_to_global)
{
    robot->setGlobalPose(Eigen::Matrix4f::Identity());

    const Eigen::Matrix4f nodeToRoot = math::Helpers::Pose(Eigen::Vector3f(100, 200, 300));

    pointCloud.changeFrame(robot->getRootNode()->getName(), robot);
    {
        auto tf = getTransformedOriginal(nodeToRoot);
        BOOST_CHECK_EQUAL_COLLECTIONS(pointCloud.cloud->begin(), pointCloud.cloud->end(),
                                      tf.points.begin(), tf.points.end());
    }

    const Eigen::Matrix4f globalPose = math::Helpers::Pose(Eigen::Vector3f(800, 600, 400));
    robot->setGlobalPose(globalPose);

    pointCloud.changeFrame(armarx::GlobalFrame, robot);
    {
        auto tf = getTransformedOriginal(globalPose * nodeToRoot);
        BOOST_CHECK_EQUAL_COLLECTIONS(pointCloud.cloud->begin(), pointCloud.cloud->end(),
                                      tf.points.begin(), tf.points.end());
    }
}

BOOST_AUTO_TEST_CASE(test_changeFrame_roundtrip)
{
    const Eigen::Matrix4f globalPose = math::Helpers::Pose(Eigen::Vector3f(800, 600, 400));
    robot->setGlobalPose(globalPose);

    pointCloud.changeFrame(robot->getRootNode()->getName(), robot);
    pointCloud.changeFrame(armarx::GlobalFrame, robot);
    pointCloud.changeFrame("Node", robot);

    BOOST_CHECK_EQUAL_COLLECTIONS(pointCloud.cloud->begin(), pointCloud.cloud->end(),
                                  original.points.begin(), original.points.end());
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_CASE(test_PointT_deduction_from_PointCloudPtrT)
{
    // This test just checks whether the following code compiles (no runtime checks).

    pcl::PointCloud<PointT>::Ptr pointCloudPtr(new pcl::PointCloud<PointT>());
    const std::string frame = "root";

    FramedPointCloud framed0(pointCloudPtr);
    FramedPointCloud framed1(pointCloudPtr, frame);

    BOOST_CHECK(true);
}


