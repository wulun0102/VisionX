#include "segments.h"

namespace visionx
{

    void tools::detail::addSegmentIndex(std::map<uint32_t, pcl::PointIndices>& segmentIndices,
                                        uint32_t label, std::size_t index)
    {
        // With [] operator, std::map default-constructs a value if it is not already
        // present, which is exactly what we need here (i.e. no need to explicitly check
        // key presence).
        segmentIndices[label].indices.push_back(static_cast<int>(index));
    }

    void tools::detail::addSegmentIndex(std::map<uint32_t, pcl::PointIndices::Ptr>& segmentIndices,
                                        uint32_t label, std::size_t index)
    {
        auto it = segmentIndices.find(label);
        if (it == segmentIndices.end())
        {
            it = segmentIndices.emplace(label, new pcl::PointIndices).first;
        }
        it->second->indices.push_back(static_cast<int>(index));
    }

}



