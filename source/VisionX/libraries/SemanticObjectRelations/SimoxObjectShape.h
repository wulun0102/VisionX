#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <SemanticObjectRelations/Shapes/Shape.h>


namespace armarx::semantic
{

    class SimoxObjectShape : public semrel::Shape
    {
    public:

        SimoxObjectShape();
        SimoxObjectShape(const VirtualRobot::ManipulationObjectPtr& object,
                         const std::string& objectClassName);

        // Shape interface
        Eigen::Vector3f getPosition() const override;
        void setPosition(Eigen::Vector3f const& position) override;

        Eigen::Quaternionf getOrientation() const override;
        void setOrientation(Eigen::Quaternionf const& orientation) override;

        semrel::TriMesh getTriMeshLocal() const override;

        semrel::AxisAlignedBoundingBox getAABBLocal() const override;
        semrel::AxisAlignedBoundingBox getAABB() const override;

        std::shared_ptr<btCollisionShape> getBulletCollisionShape(float margin) const override;

        void addMargin(float margin) override;

        std::string tagPrefix() const override;


    public:

        VirtualRobot::ManipulationObjectPtr object = nullptr;
        std::string objectClassName = "";
        std::string entityId = "";

    };

}
