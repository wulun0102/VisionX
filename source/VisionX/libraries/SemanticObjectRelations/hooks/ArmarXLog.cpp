#include "ArmarXLog.h"

#include <ArmarXCore/core/exceptions/local/UnexpectedEnumValueException.h>

namespace armarx::semantic
{

    void ArmarXLog::setAsImplementation(const std::string& tag)
    {
        semrel::LogInterface::setImplementation(std::make_shared<ArmarXLog>(tag));
    }

    ArmarXLog::ArmarXLog(const std::string& tag)
    {
        setTag(tag);
        armarx::LogSender::SetComponentName(tag);
    }


    void ArmarXLog::log(semrel::LogMetaInfo info, const std::string& message)
    {
        (*loghelper(info.file.c_str(), info.line, info.func.c_str()))
                << logLevelToMessageType(info.level)
                << message;
    }

    armarx::MessageTypeT ArmarXLog::logLevelToMessageType(semrel::LogLevel level)
    {
        switch (level)
        {
            case semrel::LogLevel::DEBUG:
                return armarx::MessageTypeT::DEBUG;
            case semrel::LogLevel::VERBOSE:
                return armarx::MessageTypeT::VERBOSE;
            case semrel::LogLevel::INFO:
                return armarx::MessageTypeT::INFO;
            case semrel::LogLevel::IMPORTANT:
                return armarx::MessageTypeT::IMPORTANT;
            case semrel::LogLevel::WARNING:
                return armarx::MessageTypeT::WARN;
            case semrel::LogLevel::ERROR:
                return armarx::MessageTypeT::ERROR;

            default:
                ARMARX_UNEXPECTED_ENUM_VALUE(semrel::LogLevel, level);
        }
    }

}
