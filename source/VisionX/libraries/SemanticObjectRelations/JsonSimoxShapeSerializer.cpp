#include "JsonSimoxShapeSerializer.h"

#include <SimoxUtility/json.h>
#include <VirtualRobot/ManipulationObject.h>

#include <SemanticObjectRelations/Shapes.h>
#include <SemanticObjectRelations/Shapes/json.h>

#include <MemoryX/libraries/helpers/VirtualRobotHelpers/ObjectClassSegmentWrapper.h>

#include "SimoxObjectShape.h"


namespace armarx::semantic
{
    JsonSimoxShapeSerializer::JsonSimoxShapeSerializer(const memoryx::ObjectClassSegmentWrapper* classSegment)
        : classSegment(classSegment)
    {
    }

    void JsonSimoxShapeSerializer::to_json(nlohmann::json& j, const SimoxObjectShape& object) const
    {
        semrel::json::to_json_base(j, object);

        j["class"] = object.objectClassName;
        j["instance"] = object.object->getName();
        j["entityId"] = object.entityId;
    }

    void JsonSimoxShapeSerializer::from_json(const nlohmann::json& j, SimoxObjectShape& object) const
    {
        const std::string objectClassName = j.at("class").get<std::string>();

        VirtualRobot::ManipulationObjectPtr manipulationObject;
        if (classSegment)
        {
            std::optional<memoryx::ObjectClassWrapper> objectClass = classSegment->getClass(objectClassName);
            if (objectClass)
            {
                const std::string instance = j.at("instance").get<std::string>();
                manipulationObject = objectClass->manipulationObject->clone(instance);
            }
        }

        object = SimoxObjectShape(manipulationObject, objectClassName);
        semrel::json::from_json_base(j, object);
        object.entityId = j.at("entityId");
    }

    void JsonSimoxShapeSerializer::registerSerializer(
        const std::shared_ptr<JsonSimoxShapeSerializer>& instance, bool overwrite)
    {
        semrel::json::ShapeSerializers::registerSerializer<SimoxObjectShape>(
            [instance](nlohmann::json & j, const SimoxObjectShape & object)
        {
            instance->to_json(j, object);
        },
        [instance](const nlohmann::json & j, SimoxObjectShape & object)
        {
            instance->from_json(j, object);
        }, overwrite);
    }

}
