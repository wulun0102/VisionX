#include "exceptions.h"

#include <sstream>


namespace visionx::voxelgrid::error::io
{

    BinaryIOError::BinaryIOError(const std::string& operation, const std::string& path,
                                 const std::string& reason, const std::ios_base::failure& causing) :
        VoxelGridIOError(
            "Failed to " + operation + " voxel grid from/to '" + path + "'.\n"
            "Reason: " + reason + "\n"
            "Caused by : " + std::string(causing.what()))
    {}

    BinaryReadError::BinaryReadError(const std::string& message, const std::ios_base::failure& causing) :
        VoxelGridIOError(message + "\nCaused by: " + std::string(causing.what()))
    {}


}


