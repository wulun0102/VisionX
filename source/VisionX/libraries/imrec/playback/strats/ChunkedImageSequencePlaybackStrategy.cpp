/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Fallback FPS to be used if the FPS are not to be found in the metadata.csv (Must not be '0'!)
#define FALLBACK_FPS 30


#include <VisionX/libraries/imrec/playback/strats/ChunkedImageSequencePlaybackStrategy.h>


// STD/STL
#include <cstring>
#include <filesystem>
#include <fstream>

// Boost
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

// OpenCV 2
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::ChunkedImageSequencePlaybackStrategy()
{
    this->playingBack = false;
    this->currentFrame = 0;
    this->frameCount = 0;
    this->metadataPath = "/dev/null";
}


visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::ChunkedImageSequencePlaybackStrategy(const std::filesystem::path& filePath)
{
    this->playingBack = false;
    this->currentFrame = 0;
    this->frameCount = 0;
    this->startPlayback(filePath);
}


visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::~ChunkedImageSequencePlaybackStrategy()
{
    this->stopPlayback();
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::isPlayingBack() const
{
    return this->playingBack;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFps() const
{
    return this->fps;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFrameCount() const
{
    return this->frameCount;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFrameHeight() const
{
    return this->frameHeight;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getFrameWidth() const
{
    return this->frameWidth;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::setCurrentFrame(unsigned int frame)
{
    this->currentFrame = frame;
}


unsigned int
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getCurrentFrame() const
{
    return this->currentFrame;
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::hasNextFrame() const
{
    return this->currentFrame < this->frameCount;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::startPlayback(const std::filesystem::path& filePath)
{
    // Find canonical path to metadata.csv given the parameter
    if (std::filesystem::is_directory(filePath))
    {
        this->metadataPath = std::filesystem::canonical(filePath);
    }
    else
    {
        this->metadataPath = std::filesystem::canonical(filePath.parent_path());
    }

    // Init metadata member from metadata.csv
    this->initMetadata();

    // Init chunked image sequence recording related variables required to process the frames
    this->initFps();
    this->initExtension();
    this->initFramesPerChunk();
    this->initFrameCount();
    this->initFrameHeight();
    this->initFrameWidth();

    // Build path for each individual frame
    for (unsigned int currentFrameNumber = 0; currentFrameNumber < this->frameCount; ++currentFrameNumber)
    {
        const unsigned int currentChunkNumber = currentFrameNumber / framesPerChunk;
        const std::filesystem::path currentChunk("chunk_" + std::to_string(currentChunkNumber));
        const std::filesystem::path frameFile = std::filesystem::path("frame_" + std::to_string(currentFrameNumber)).replace_extension(this->extension);

        const std::filesystem::path currentFrame = std::filesystem::canonical(this->metadataPath / currentChunk / frameFile);
        this->framePaths.push_back(currentFrame);
    }

    this->playingBack = true;
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getNextFrame(void* buffer)
{
    // Return failure on invalid frame
    if (this->currentFrame >= this->frameCount)
    {
        return false;
    }

    // Load frame with OpenCV
    cv::Mat frame;
    this->getNextFrame(frame);

    // Convert to RGB and write data to buffer
    cv::cvtColor(frame, frame, cv::COLOR_BGR2RGB);
    std::memcpy(buffer, frame.data, this->frameHeight * this->frameWidth * static_cast<unsigned int>(frame.channels()));

    // Return success
    return true;
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getNextFrame(::CByteImage& buffer)
{
    return this->getNextFrame(buffer.pixels);
}


bool
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::getNextFrame(cv::Mat& buffer)
{
    buffer = cv::imread(this->framePaths[this->currentFrame++].string());

    // Return failure if OpenCV encountered an error
    if (buffer.data == nullptr)
    {
        return false;
    }

    // Check if the dimensions fit the expected image size
    if (static_cast<unsigned int>(buffer.size().height) != this->frameHeight or static_cast<unsigned int>(buffer.size().width) != this->frameWidth)
    {
        return false;
    }

    return true;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::stopPlayback()
{
    this->playingBack = false;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initMetadata()
{
    // Read contents
    std::ifstream metadatacsv((this->metadataPath / "metadata.csv").string());
    std::string line;
    while (std::getline(metadatacsv, line))
    {
        boost::tokenizer<boost::escaped_list_separator<char>> tokenizer(line);
        std::vector<std::string> tokens(tokenizer.begin(), tokenizer.end());

        const std::string varName = tokens[0];
        const std::string varType = tokens[1];
        const std::string varValue = tokens[2];

        this->metadata[varName] = std::make_tuple(varType, varValue);
    }
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFps()
{
    unsigned int candidate = 0;

    if (this->metadata.find("fps") != this->metadata.end())
    {
        candidate = this->fps = boost::lexical_cast<unsigned int>(std::get<1>(this->metadata.at("fps")));

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "FPS cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'fps' entry found in the metadata.csv. Assuming '" << FALLBACK_FPS << "' as default. FPS cannot be derived from an image sequence and are"
                       "expected to be manifested in the metadata.csv file. If the assumed value does not reflect the actual FPS, update the 'fps' variable in the metadata.csv";

        candidate = FALLBACK_FPS; // unchecked, but must not be 0
        this->updateMetadata("fps", "unsigned int", std::to_string(candidate));
    }

    this->fps = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initExtension()
{
    std::string candidate = "";

    if (this->metadata.find("extension") != this->metadata.end())
    {
        candidate = std::get<1>(this->metadata.at("extension"));

        ARMARX_CHECK_EXPRESSION(candidate != "") << "File extension cannot be empty";
    }
    else
    {
        ARMARX_WARNING << "No 'extension' entry found in the metadata.csv. Trying to derive the value now...";

        const std::filesystem::path probeChunk = this->metadataPath / "chunk_0";

        for (const std::filesystem::directory_entry& de : boost::make_iterator_range(std::filesystem::directory_iterator(probeChunk), {}))
        {
            const std::string currentStem = de.path().stem().string();
            const std::string currentExtension = de.path().extension().string();
            const std::string frameTemplate = "frame_";

            if (boost::algorithm::starts_with(currentStem, frameTemplate) and boost::algorithm::starts_with(currentExtension, ".") and currentExtension != ".")
            {
                candidate = currentExtension;
                break;
            }
        }

        ARMARX_CHECK_EXPRESSION(candidate != "") << "Could not determine the file extension. Update the metadata.csv file manually for the variable"
                "'extension' (type 'string') with the corresponding value";

        ARMARX_INFO << "Determined 'extension' to be '" << candidate << "'";

        this->updateMetadata("extension", "string", candidate);
    }

    this->extension = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFramesPerChunk()
{
    unsigned int candidate = 0;

    if (this->metadata.find("framesPerChunk") != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(this->metadata.at("framesPerChunk")));

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Amount of frames per chunk cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'framesPerChunk' entry found in the metadata.csv. Trying to derive the value now...";

        candidate = 0;
        std::filesystem::path file = std::filesystem::path("frame_0").replace_extension(this->extension);
        while (std::filesystem::exists(this->metadataPath / "chunk_0" / file))
        {
            file = std::filesystem::path("frame_" + std::to_string(++candidate)).replace_extension(this->extension);
        }

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Could not determine the amount of frames per chunk. Update the metadata.csv file manually for the variable"
                                               "'framesPerChunk' (type 'unsigned int') with the corresponding value.";

        ARMARX_INFO << "Determined 'framesPerChunk' to be '" << candidate << "'";

        this->updateMetadata("framesPerChunk", "unsigned int", std::to_string(candidate));
    }

    this->framesPerChunk = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFrameCount()
{
    unsigned int candidate = 0;

    if (this->metadata.find("frameCount") != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(this->metadata.at("frameCount")));

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Amount of frames cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frameCount' entry found in the metadata.csv. Trying to derive the value now...";

        candidate = 0;
        std::filesystem::path chunk = "chunk_0";
        unsigned int chunkNumber = 0;
        std::filesystem::path file = std::filesystem::path("frame_0").replace_extension(this->extension);
        while (std::filesystem::exists(this->metadataPath / chunk))
        {
            while (std::filesystem::exists(this->metadataPath / chunk / file))
            {
                file = std::filesystem::path("frame_" + std::to_string(++candidate)).replace_extension(this->extension);
            }

            chunk = "chunk_" + std::to_string(++chunkNumber);
        }

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Could not determine the amount of frames. Update the metadata.csv file manually for the variable"
                                               "'frameCount' (type 'unsigned int') with the corresponding value";

        ARMARX_INFO << "Determined 'frameCount' to be '" << candidate << "'";

        this->updateMetadata("frameCount", "unsigned int", std::to_string(candidate));
    }

    this->frameCount = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFrameHeight()
{
    unsigned int candidate = 0;

    if (this->metadata.find("frameHeight") != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(this->metadata.at("frameHeight")));

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Frame height cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frameHeight' entry found in the metadata.csv. Trying to derive the value now...";

        const std::filesystem::path chunk = "chunk_0";
        const std::filesystem::path frame = std::filesystem::path("frame_0").replace_extension(this->extension);
        const std::filesystem::path probe = this->metadataPath / chunk / frame;
        cv::Mat frameImage = cv::imread(probe.string());

        ARMARX_CHECK_EXPRESSION(frameImage.data != nullptr) << "Failed loading first frame. Image file corrupted?";

        candidate = static_cast<unsigned int>(frameImage.size().height);

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Could not determine the frame height. Update the metadata.csv file manually for the variable"
                                               "'frameHeight' (type 'unsigned int') with the corresponding value";

        ARMARX_INFO << "Determined 'frameHeight' to be '" << candidate << "'";

        this->updateMetadata("frameHeight", "unsigned int", std::to_string(candidate));
    }

    this->frameHeight = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::initFrameWidth()
{
    unsigned int candidate = 0;

    if (this->metadata.find("frameWidth") != this->metadata.end())
    {
        candidate = boost::lexical_cast<unsigned int>(std::get<1>(this->metadata.at("frameWidth")));

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Frame width cannot be '0'";
    }
    else
    {
        ARMARX_WARNING << "No 'frameWidth' entry found in the metadata.csv. Trying to derive the value now...";

        const std::filesystem::path chunk = "chunk_0";
        const std::filesystem::path frame = std::filesystem::path("frame_0").replace_extension(this->extension);
        const std::filesystem::path probe = this->metadataPath / chunk / frame;
        cv::Mat frameImage = cv::imread(probe.string());

        ARMARX_CHECK_EXPRESSION(frameImage.data != nullptr) << "Failed loading first frame. Image file corrupted?";

        candidate = static_cast<unsigned int>(frameImage.size().width);

        ARMARX_CHECK_EXPRESSION(candidate > 0) << "Could not determine the frame width. Update the metadata.csv file manually for the variable"
                                               "'frameWidth' (type 'unsigned int') with the corresponding value";

        ARMARX_INFO << "Determined 'frameWidth' to be '" << candidate << "'";

        this->updateMetadata("frameWidth", "unsigned int", std::to_string(candidate));
    }

    this->frameWidth = candidate;
}


void
visionx::imrec::strats::ChunkedImageSequencePlaybackStrategy::updateMetadata(const std::string& varName, const std::string& varType, const std::string& varValue)
{
    if (this->metadata.find(varName) != this->metadata.end())
    {
        return;
    }

    ARMARX_INFO << "Updating metadata.csv file with missing variable '" << varName << "' (type '" << varType << "') with value '" << varValue << "'";

    std::ofstream metadatacsv((this->metadataPath / "metadata.csv").string(), std::ios::out | std::ios::app);
    metadatacsv << varName << "," << varType << "," << varValue << "\n";
}
