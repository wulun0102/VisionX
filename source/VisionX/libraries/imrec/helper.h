/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <chrono>
#include <string>


class CByteImage;


namespace cv
{
    class Mat;
}


namespace visionx::imrec
{

    /**
     * @brief Converts an IVT CByteImage to OpenCV's Mat
     * @param in CByteImage
     * @param out Output parameter
     */
    void convert(const CByteImage& in, cv::Mat& out);

    /**
     * @brief Converts an OpenCV Mat to IVT's CByteImage
     * @param in cv::Mat
     * @param out Output parameter
     */
    void convert(const cv::Mat& in, CByteImage& out);


    std::string datetime_to_string(std::chrono::microseconds ts);

    std::string timestamp_to_string(std::chrono::microseconds ts);

}
