/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <filesystem>
#include <string>

// ArmarX
#include <VisionX/libraries/imrec/record/AbstractRecordingStrategy.h>


namespace visionx::imrec
{

    /**
     * An object of this class behaves likee a normal recording, but is in fact a sequence of images
     */
    class AbstractSequencedRecordingStrategy :
        public AbstractRecordingStrategy
    {

    private:

        std::filesystem::path m_ext;

        /**
         * @brief Name of the current chunk (part of the path to overcome file system limitations)
         */
        std::filesystem::path m_frames_chunk;

    public:

        /**
         * @brief Default constructor to manually start the recording
         */
        AbstractSequencedRecordingStrategy();

        AbstractSequencedRecordingStrategy(const std::filesystem::path& filePath, const std::filesystem::path& ext);

        /**
         * @brief Destruct the recording strategy
         */
        ~AbstractSequencedRecordingStrategy() override;

        void startRecording() override;

        std::filesystem::path getDotExtension() const override;

        std::filesystem::path getMetadataPath() const override;

    protected:

        /**
         * @brief Returns the next sequenced full path and increments the sequence number
         * @return Next sequenced full path
         */
        std::filesystem::path deriveFramePath(const unsigned int sequence_number, const std::string& frame_name);

    };

}
