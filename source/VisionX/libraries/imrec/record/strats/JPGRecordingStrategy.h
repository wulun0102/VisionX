/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <filesystem>
#include <string>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/libraries/imrec/record/AbstractSequencedRecordingStrategy.h>


namespace visionx::imrec::strats
{

    /**
     * Concrete strategy for a JPG image recording
     */
    class JPGRecordingStrategy :
        public AbstractSequencedRecordingStrategy
    {

    protected:

        unsigned int m_jpg_quality;

    public:

        JPGRecordingStrategy();
        JPGRecordingStrategy(const std::filesystem::path& filePath, const std::string& name, unsigned int jpg_quality = 95);
        ~JPGRecordingStrategy() override;

        void startRecording() override;
        void recordFrame(const cv::Mat& frame, std::chrono::microseconds timestamp) override;

    };

}
