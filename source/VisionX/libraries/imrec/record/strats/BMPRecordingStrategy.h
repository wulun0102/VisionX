/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/libraries/imrec/record/AbstractSequencedRecordingStrategy.h>


namespace visionx::imrec::strats
{

    /**
     * Concrete strategy for a BMP image recording
     */
    class BMPRecordingStrategy :
        public AbstractSequencedRecordingStrategy
    {

    public:

        BMPRecordingStrategy();
        BMPRecordingStrategy(const std::filesystem::path& filePath, const std::string& name);
        ~BMPRecordingStrategy() override;

        void recordFrame(const CByteImage& frame, std::chrono::microseconds timestamp) override;

    };

}
