/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <chrono>
#include <filesystem>
#include <fstream>
#include <functional>
#include <memory>
#include <set>
#include <string>
#include <string_view>
#include <tuple>


class CByteImage;


namespace cv
{
    class Mat;
}


namespace visionx::imrec
{

    /**
     * Abstract interface of a recording strategy
     */
    class AbstractRecordingStrategy
    {

    protected:

        /**
         * @brief Path to where the recording file should be written to
         */
        std::filesystem::path m_file_path;

    private:

        /**
         * @brief Flag to indicate whether the recording already started
         */
        bool m_recording;

        /**
         * @brief File stream for the metadata file
         */
        std::ofstream m_metadata_file;

        /**
         * @brief List of already written metadata variable names
         */
        std::set<std::string> m_written_metadata_variables;

        /**
         * @brief Frame number which is additionally displayed in the filename
         */
        unsigned int m_sequence_number = 0;

        std::chrono::microseconds m_reference_time{0};

    public:

        /**
         * @brief Default constructor to start the recording manually
         */
        AbstractRecordingStrategy();

        /**
         * @brief Constructor for any recording strategy, immediately starting the recording
         * @param filePath Path to where the recording file should be written to
         * @param fps Amount of frames being recorded per second
         */
        AbstractRecordingStrategy(const std::filesystem::path& filePath);

        /**
         * @brief Destructor
         */
        virtual ~AbstractRecordingStrategy();

        /**
         * @brief Indicates whether this instance is already initialised for recording
         * @return True if it is initialised and ready to record, false if not
         */
        virtual bool isRecording() const;

        /**
         * @brief Starts the recording manually if constructed empty
         * @param filePath Path to where the recording file should be written to
         * @param fps Amount of frames being recorded per second
         */
        virtual void startRecording();

        /**
         * @brief Adds the given frame to the recording
         * @param frame Frame to be added
         */
        virtual void recordFrame(const CByteImage& frame, std::chrono::microseconds timestamp);

        /**
         * @brief Adds the given frame to the recording
         * @param frame Frame to be added
         */
        virtual void recordFrame(const cv::Mat& frame, std::chrono::microseconds timestamp);

        /**
         * @brief Stops the recording
         */
        virtual void stopRecording();

        /**
         * @brief Gets the raw file path for the recording as configured
         * @return Raw file path for the recording as configureds
         */
        virtual std::filesystem::path getFilePath() const;

        /**
         * @brief Gets the path to the recording without filename
         * @return Path to the recording without filename
         */
        virtual std::filesystem::path getPath() const;

        /**
         * @brief Gets the stem of the configured file (filename without extension)
         * @return Stem of the configured file
         */
        virtual std::filesystem::path getStem() const;

        /**
         * @brief Gets the extension plus preceeded dot of the configured file (e.g. ".avi")
         * @return Extention plus preceeded dot of the configured file
         */
        virtual std::filesystem::path getDotExtension() const;

        virtual std::filesystem::path getMetadataPath() const;

        virtual std::tuple<unsigned int, std::string> writeMetadataFrame(const CByteImage& frame, std::chrono::microseconds timestamp);

        virtual std::tuple<unsigned int, std::string> writeMetadataFrame(const cv::Mat& frame, std::chrono::microseconds timestamp);

        virtual void writeMetadataDatetime(const std::string& var_name, std::chrono::microseconds timestamp);

        virtual void writeMetadataLine(const std::string& var_name, std::string_view var_type, std::string_view var_value);

    private:

        virtual std::tuple<unsigned int, std::string> writeMetadataFrameFileInfo(std::chrono::microseconds timestamp);

    };


    /**
     * Convenience alias for any recording strategy
     */
    using Recording = std::shared_ptr<AbstractRecordingStrategy>;

}
