/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/imrec/helper.h>


// STD/STL
#include <cstring>
#include <iomanip>
#include <sstream>

// OpenCV 2
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// IVT
#include <Image/ByteImage.h>


void
visionx::imrec::convert(const CByteImage& in, cv::Mat& out)
{
    // Initialise RGB OpenCV image from CByteImage
    cv::Mat cv_frame(cv::Size(in.width, in.height), CV_8UC3, CV_MAT_CN(CV_8UC3));
    std::memcpy(cv_frame.data, in.pixels, static_cast<unsigned int>(in.width * in.height * in.bytesPerPixel));

    // Convert to BGR OpenCV image (OpenCV assumes this format by default)
    cv::cvtColor(cv_frame, cv_frame, cv::COLOR_RGB2BGR);
    out = cv_frame;
}


void
visionx::imrec::convert(const cv::Mat& in, CByteImage& out)
{
    // Create a cv::Mat copy and convert to RGB format
    cv::Mat frame_cpy(in.size(), in.type());
    cv::cvtColor(in, frame_cpy, cv::COLOR_BGR2RGB);

    // Initialise CByteImage and copy pixel data
    out.Set(in.size().width, in.size().height, ::CByteImage::ImageType::eRGB24);
    std::memcpy(out.pixels, frame_cpy.data, static_cast<unsigned int>(in.size().height * in.size().width * in.channels()));
}


std::string
visionx::imrec::datetime_to_string(std::chrono::microseconds ts)
{
    std::chrono::time_point<std::chrono::system_clock> tp{ts};
    time_t time = std::chrono::system_clock::to_time_t(tp);

    struct tm tstruct;
    tstruct = *localtime(&time);  // use locale time
    //tstruct = *gmtime(&time);     // use UTC time

    int time_us = ts.count() % 1000;
    int time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(ts).count() % 1000;
    int time_s = tstruct.tm_sec;
    int time_m = tstruct.tm_min;
    int time_h = tstruct.tm_hour;

    int date_d = tstruct.tm_mday;
    int date_m = tstruct.tm_mon + 1;
    int date_y = tstruct.tm_year + 1900;

    std::stringstream ss;
    ss << std::setw(4) << std::setfill('0') << date_y << "-"; // year
    ss << std::setw(2) << std::setfill('0') << date_m << "-";   // month
    ss << std::setw(2) << std::setfill('0') << date_d << "_";   // day

    ss << std::setw(2) << std::setfill('0') << time_h << "-";   // hour
    ss << std::setw(2) << std::setfill('0') << time_m << "-";   // minute
    ss << std::setw(2) << std::setfill('0') << time_s << "-";   // second

    ss << std::setw(3) << std::setfill('0') << time_ms;         // millisecond
    ss << std::setw(3) << std::setfill('0') << time_us;         // microsecond

    return ss.str();
}


std::string
visionx::imrec::timestamp_to_string(std::chrono::microseconds ts)
{
    int us = ts.count() % 1000;
    int ms = std::chrono::duration_cast<std::chrono::milliseconds>(ts).count() % 1000;
    int s = std::chrono::duration_cast<std::chrono::seconds>(ts).count();
    int m = std::chrono::duration_cast<std::chrono::minutes>(ts).count();
    int h = std::chrono::duration_cast<std::chrono::hours>(ts).count();

    std::stringstream ss;
    ss << std::setw(2) << std::setfill('0') << h << "_";   // hour
    ss << std::setw(2) << std::setfill('0') << m << "_";   // minute
    ss << std::setw(2) << std::setfill('0') << s << "_";   // second

    ss << std::setw(3) << std::setfill('0') << ms;         // millisecond
    ss << std::setw(3) << std::setfill('0') << us;         // microsecond

    return ss.str();
}
