/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::imrec
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <filesystem>
#include <map>

// ArmarX
#include <VisionX/libraries/imrec/playback/AbstractPlaybackStrategy.h>
#include <VisionX/libraries/imrec/record/AbstractRecordingStrategy.h>


class CByteImage;


namespace cv
{
    class Mat;
}


namespace visionx::imrec
{


    /**
     * @brief Supported recording Formats.
     *
     * @note If you add new formats, also add them to the registry (declared in an anonymous
     *       namespace) in public_api.cpp.
     */
    enum class Format
    {
        bmp_img_seq,
        png_img_seq,
        png_fast_img_seq,
        png_mt_img_seq,
        png_fast_mt_img_seq,
        jpg_img_seq,
        avi_default,
        mp4_default
    };


    enum class Framerate
    {
        static_fps,
        dynamic_fps
    };


    enum class Compression
    {
        lossy,
        lossless
    };


    struct RegistryEntry
    {
        /**
         * @brief Identifier to use for long-term storage purposes etc.
         */
        std::string id;

        /**
         * @brief Human readable identifier for UI's or outputs.  May change, so do not use as an
         *        identifier for long-term storage.
         */
        std::string hr_name;

        /**
         * @brief Framerate handling
         */
        Framerate framerate;

        /**
         * @brief Compression handling
         */
        Compression compression;
    };


    const std::string& format2str(const Format format);


    const std::string& format2hrstr(const Format format);


    Format str2format(const std::string& format_str);


    std::vector<Format> getFormats();


    std::map<Format, RegistryEntry> getFormatsMap();


    bool isFormatLossless(Format format);


    bool isFormatDynamicFramerate(Format format);


    /**
     * @brief Instantiates and returns a new playback strategy which is capable of replaying the file or collection at path
     * @param path Path to the recording file or collection (folder, glob, ...)
     * @return Concrete playback strategy capable of replaying the recording at path
     */
    visionx::imrec::Playback newPlayback(const std::filesystem::path& path);


    visionx::imrec::Recording newRecording(const std::filesystem::path& path, const std::string& name, const Format format, double fps);


    /**
     * @brief Creates a new recording given the file extension and additional parameters
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     * @return Recording object
     */
    visionx::imrec::Recording newRecording(const std::filesystem::path& filePath, double fps);


    /**
     * @brief Takes a snapshot using the default recording method for snapshots
     * @param filePath Path to where the recording file should be written to
     * @param image The image to be saved
     */
    void takeSnapshot(const CByteImage& image, const std::filesystem::path& filePath);


    /**
     * @brief Takes a snapshot using the default recording method for snapshots
     * @param filePath Path to where the recording file should be written to
     * @param image The image to be saved
     */
    void takeSnapshot(const cv::Mat& image, const std::filesystem::path& filePath);

}
