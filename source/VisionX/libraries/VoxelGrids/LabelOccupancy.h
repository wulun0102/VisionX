#pragma once

#include "LabelOccupancy/VoxelGrid.h"
#include "LabelOccupancy/Visualizer.h"


namespace visionx
{
    using LabelOccupancyVoxel = voxelgrid::LabelOccupancy::Voxel;
    using LabelOccupancyVoxelGrid = voxelgrid::LabelOccupancy::VoxelGrid;
    using LabelOccupancyVisualizer = voxelgrid::LabelOccupancy::Visualizer;
}
