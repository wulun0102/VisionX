#include "Visualizer.h"

#include <pcl/common/colors.h>
#include <pcl/point_types.h>  // for pcl::RGB

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace visionx::voxelgrid::LabelOccupancy
{

    Visualizer::Visualizer()
        = default;

    Visualizer::Visualizer(
        const armarx::DebugDrawerTopic& debugDrawer, const std::string& layer) :
        voxelgrid::Visualizer<Voxel>(debugDrawer, layer)
    {}

    Visualizer::~Visualizer()
        = default;


    bool Visualizer::isVisible(const Visualizer::VoxelVisuData& visu) const
    {
        return visu.voxel.isOccupied();
    }

    void Visualizer::drawVoxel(const Visualizer::VoxelVisuData& visu)
    {
        ARMARX_CHECK(visu.voxel.isOccupied());

        const Label label = visu.voxel.getLabel();

        const pcl::RGB rgb = pcl::GlasbeyLUT::at(static_cast<unsigned int>(label)
                             % pcl::GlasbeyLUT::size());

        const armarx::DrawColor color = drawer().toDrawColor(rgb, alpha, true);

        drawer().drawBox({getVoxelLayer(), visu.name + "_" + std::to_string(label)},
                         visu.pos, visu.ori, visu.extents, color);
    }

    float Visualizer::getAlpha() const
    {
        return alpha;
    }

    void Visualizer::setAlpha(float value)
    {
        ARMARX_CHECK_LESS_EQUAL(0, value);
        ARMARX_CHECK_LESS_EQUAL(value, 1);

        this->alpha = value;
    }


}
