/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools::Tests
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE VisionX::tools::TypeMapping::ObjectColorType
#define ARMARX_BOOST_TEST
#include <VisionX/Test.h>

// IVT
#include <Structs/Structs.h>
#include <Structs/ObjectDefinitions.h>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// VisionXInterface
#include <VisionX/interface/units/ObjectRecognitionUnit.h>

BOOST_AUTO_TEST_CASE(ObjectTypeIvtToVisionXMappingTest)
{
    BOOST_CHECK(visionx::tools::convert(eCompactObject)  == visionx::types::eCompactObject);
    BOOST_CHECK(visionx::tools::convert(eTexturedObject) == visionx::types::eTexturedObject);
    BOOST_CHECK(visionx::tools::convert(eHead)           == visionx::types::eHead);
    BOOST_CHECK(visionx::tools::convert(eLeftHand)       == visionx::types::eLeftHand);
    BOOST_CHECK(visionx::tools::convert(eRightHand)      == visionx::types::eRightHand);
    BOOST_CHECK(visionx::tools::convert(eUnknownObject)  == visionx::types::eUnknownObject);
}


BOOST_AUTO_TEST_CASE(ObjectTypeVisionXToIvtMappingTest)
{
    BOOST_CHECK(visionx::tools::convert(visionx::types::eUnknownObject)  == eUnknownObject);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eTexturedObject) == eTexturedObject);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eHead)           == eHead);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eLeftHand)       == eLeftHand);
    BOOST_CHECK(visionx::tools::convert(visionx::types::eUnknownObject)  == eUnknownObject);
}
