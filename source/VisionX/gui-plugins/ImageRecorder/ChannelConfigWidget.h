/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::ImageRecorder
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Qt
#include <QWidget>

// ArmarX
#include <VisionX/interface/core/ImageProviderInterface.h>
#include <VisionX/gui-plugins/ImageRecorder/ui_ChannelConfigWidget.h>
#include <VisionX/gui-plugins/ImageRecorder/ImageRecorderWidgetController.h>


namespace visionx
{

    class ChannelConfigWidget : public QWidget
    {

        Q_OBJECT

    private:

        Ui::channel_config_widget m_widget;
        std::string m_channel_name;
        visionx::imrec::ChannelPreferences m_channel_prefs;

    public:

        ChannelConfigWidget(QWidget* parent, const visionx::imrec::ChannelConfig& channel_config, const visionx::imrec::ChannelPreferences& channel_prefs);

        virtual ~ChannelConfigWidget();

        const std::string& getChannelName() const;

        void confirmChannelName(const std::string& new_name);

        void rejectChannelName(const std::string& new_name);

        void setChannelPreferences(const imrec::ChannelPreferences& channel_prefs);

        imrec::ChannelConfig toChannelConfig(bool disabled) const;

    private:

        void update_file_formats(bool require_lossless);

        void set_visible_file_fps(const QString& format_str);

        void set_visible_file_fps(bool visible);

    signals:

        void channelRenamed(const std::string& old_name, const std::string& new_name);

    private slots:

        void channel_name_changed(const QString& new_name);

        void lossless_state_changed(int state);

        void file_format_changed(const QString& new_format_str);

    };

}
