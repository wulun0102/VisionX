armarx_set_target("ImageRecorderGuiPlugin")

set(SOURCES
    ChannelConfigWidget.cpp
    ImageProviderConfigWidget.cpp
    ImageRecorderGuiPlugin.cpp
    ImageRecorderWidgetController.cpp
)

set(HEADERS
    ChannelConfigWidget.h
    ImageProviderConfigWidget.h
    ImageRecorderGuiPlugin.h
    ImageRecorderWidgetController.h
)

set(GUI_MOC_HDRS "${HEADERS}")

set(GUI_UIS
    ChannelConfigWidget.ui
    ImageProviderConfigWidget.ui
    ImageRecorderWidget.ui
)

set(GUI_RCS
    icons.qrc
)

set(COMPONENT_LIBS
    ${COMPONENT_LIBS}
    ArmarXGuiBase
    SimpleConfigDialog
    visionx-imrec
    ImageRecordingManagerInterfaces
)

set(LIB_NAME ImageRecorderGuiPlugin)

if(ArmarXGui_FOUND)
    armarx_gui_library("${LIB_NAME}" "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "${GUI_RCS}" "${COMPONENT_LIBS}")
endif()
