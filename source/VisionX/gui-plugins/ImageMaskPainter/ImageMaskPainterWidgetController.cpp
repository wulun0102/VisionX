/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::ImageMaskPainterWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include <QImage>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <VisionX/tools/ImageUtil.h>

#include "ImageMaskPainterWidgetController.h"

using namespace armarx;

ImageMaskPainterWidgetController::ImageMaskPainterWidgetController()
{
    _widget.setupUi(getWidget());

    _maskEditor = new ImageMaskEditor;
    _widget.horizontalLayoutImageMonitors->addWidget(_maskEditor);

    _maskEditor->setPenCircleVisible(true);
    QImage bg(512, 512, QImage::Format_ARGB32);
    bg.fill(Qt::red);
    _maskEditor->setBackgroundImage(bg);
    _maskEditor->setPenWidth(_widget.spinBoxPenWidth->value());
    _maskEditor->setMaskAlpha(_widget.spinBoxPenAlpha->value());

    updatePenColor();

    connect(_maskEditor, SIGNAL(maskUpdateFinished()), this, SLOT(maskUpdated()));
    connect(_maskEditor, SIGNAL(arrowDrawn(const QPoint&, const QPoint&)), this, SLOT(arrowDrawn(const QPoint&, const QPoint&)));

    connect(_widget.spinBoxPenWidth, SIGNAL(valueChanged(int)), _maskEditor, SLOT(setPenWidth(int)));
    connect(_widget.spinBoxPenAlpha, SIGNAL(valueChanged(int)), _maskEditor, SLOT(setMaskAlpha(int)));
    connect(_widget.pushButtonClear, SIGNAL(clicked()), _maskEditor, SLOT(clearMaskImage()));

    connect(_widget.spinBoxImageIndex, SIGNAL(valueChanged(int)), this, SLOT(imageIndexChanged(int)));
    connect(_widget.checkBoxErase, SIGNAL(clicked()), this, SLOT(updatePenColor()));

    startTimer(1);
}

ImageMaskPainterWidgetController::~ImageMaskPainterWidgetController()
    = default;

QPointer<QDialog> ImageMaskPainterWidgetController::getConfigDialog(QWidget* parent)
{
    if (!_dialog)
    {
        _dialog = new SimpleConfigDialog(parent);
        _dialog->addProxyFinder<visionx::ImageProviderInterfacePrx>({"ImageProvider", "ImageProvider", "Image*|*Provider"});
    }
    return qobject_cast<SimpleConfigDialog*>(_dialog);
}

void ImageMaskPainterWidgetController::onInitImageProcessor()
{
    usingImageProvider(_imageProviderName);
}

void ImageMaskPainterWidgetController::configured()
{
    _imageProviderName = _dialog->getProxyName("ImageProvider");
}

void ImageMaskPainterWidgetController::loadSettings(QSettings* settings)
{
    _imageProviderName = settings->value("_imageProviderName", "OpenNIPointCloudProvider").toString().toStdString();
}

void ImageMaskPainterWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("_imageProviderName", QString::fromStdString(_imageProviderName));
}

void ImageMaskPainterWidgetController::onConnectImageProcessor()
{
    if (!_providerImagesOwner.empty())
    {
        return;
    }
    _imageProviderInfo = getImageProvider(_imageProviderName, true);
    _imageProvider = _imageProviderInfo.proxy;
    ARMARX_CHECK_GREATER(_imageProviderInfo.numberImages, 0);
    ARMARX_CHECK_EQUAL(_imageProviderInfo.imageFormat.type, visionx::ImageType::eRgb);
    //reserve buffers
    _providerImagesOwner.reserve(_imageProviderInfo.numberImages);
    _providerImages.reserve(_imageProviderInfo.numberImages);
    for (int i = 0; i < _imageProviderInfo.numberImages; ++i)
    {
        _providerImagesOwner.emplace_back(visionx::tools::createByteImage(_imageProviderInfo));
        _providerImages.emplace_back(static_cast<void*>(_providerImagesOwner.back()->pixels));
    }
    _numberOfImages = _imageProviderInfo.numberImages;
    _resultImage.reset(visionx::tools::createByteImage(_imageProviderInfo));
    enableResultImages(1, _imageProviderInfo.imageFormat.dimension, visionx::ImageType::eRgb, _imageProviderName + "_Mask");

    {
        _imageProviderReferenceFrame = "Global";
        auto frameprov = visionx::ReferenceFrameInterfacePrx::checkedCast(_imageProvider);
        if (frameprov)
        {
            _imageProviderReferenceFrame = frameprov->getReferenceFrame();
        }
    }
    {
        _imageProviderAreImagesUndistorted = true;

        auto mcalibprov = visionx::MonocularCalibrationCapturingProviderInterfacePrx::checkedCast(_imageProvider);
        auto scalibprov = visionx::StereoCalibrationInterfacePrx::checkedCast(_imageProvider);
        if (scalibprov)
        {
            _imageProviderCalibration = scalibprov->getStereoCalibration();
            _imageProviderAreImagesUndistorted = scalibprov->getImagesAreUndistorted();
        }
        else if (mcalibprov)
        {
            ARMARX_WARNING << "only monoocular upstream calibration! duplicating it and using identity matices";
            auto mono = mcalibprov->getCalibration();
            _imageProviderCalibration.calibrationLeft = mono;
            _imageProviderCalibration.calibrationRight = mono;
            _imageProviderCalibration.rectificationHomographyLeft = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
            _imageProviderCalibration.rectificationHomographyRight = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

            _imageProviderAreImagesUndistorted = true;
        }
        else
        {
            ARMARX_WARNING << "no upstream calibration! using default values";
            _imageProviderCalibration.calibrationLeft.cameraParam.distortion = {0, 0};
            _imageProviderCalibration.calibrationLeft.cameraParam.focalLength = {50, 50};
            _imageProviderCalibration.calibrationLeft.cameraParam.height = _imageProviderInfo.imageFormat.dimension.height;
            _imageProviderCalibration.calibrationLeft.cameraParam.width = _imageProviderInfo.imageFormat.dimension.width;
            _imageProviderCalibration.calibrationLeft.cameraParam.principalPoint =
            {
                _imageProviderInfo.imageFormat.dimension.width / 2.f,
                _imageProviderInfo.imageFormat.dimension.height / 2.f
            };
            _imageProviderCalibration.calibrationLeft.cameraParam.rotation = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
            _imageProviderCalibration.calibrationLeft.cameraParam.translation = {0, 0, 0};

            _imageProviderCalibration.calibrationRight = _imageProviderCalibration.calibrationLeft;
            _imageProviderCalibration.rectificationHomographyLeft = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
            _imageProviderCalibration.rectificationHomographyRight = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
        }
    }
}

void ImageMaskPainterWidgetController::process()
{
    if (!waitForImages(_imageProviderName, 1000))
    {
        return;
    }
    getImages(_providerImages.data());
    const CByteImage& img = *(_providerImagesOwner.at(_imageIndex));
    {
        std::lock_guard<std::mutex> guard {_currentImageMutex};
        _currentImage = QImage(img.width, img.height, QImage::Format_RGB888);
        ARMARX_CHECK_NOT_NULL(_currentImage.bits());
        ARMARX_CHECK_NOT_NULL(img.pixels);
        std::memcpy(_currentImage.bits(), img.pixels, 3 * img.width * img.height);
        _currentImageDirty = true;
    }
    ARMARX_INFO << deactivateSpam() << "received image";
}

void ImageMaskPainterWidgetController::timerEvent(QTimerEvent*)
{
    _widget.spinBoxImageIndex->setMaximum(std::max(1, _numberOfImages.load()) - 1);
    if (!_currentImageDirty)
    {
        return;
    }
    {
        std::lock_guard<std::mutex> guard {_currentImageMutex};
        _maskEditor->setBackgroundImage(_currentImage);
        _currentImageDirty = false;
    }
    ARMARX_INFO << deactivateSpam() << "updated displayed image";
    if (_maskSet)
    {
        ARMARX_INFO << deactivateSpam() << "send new mask";
        auto ptr = _resultImage.get();
        provideResultImages(&ptr);
    }
}

QString ImageMaskPainterWidgetController::GetWidgetName()
{
    return "VisionX.ImageMaskPainter";
}

void ImageMaskPainterWidgetController::updatePenColor()
{
    auto color = _widget.checkBoxErase->isChecked() ? _maskEditor->transparentColor() : QColor(0, 0, 255);
    _maskEditor->setMaskColor(color);
}

void ImageMaskPainterWidgetController::imageIndexChanged(int i)
{
    _imageIndex = _widget.spinBoxImageIndex->value();
}

void ImageMaskPainterWidgetController::maskUpdated()
{
    if (!_resultImage)
    {
        //oninit did not run
        return;
    }
    QSize sz = _maskEditor->imageSize();
    if (sz.width() != _resultImage->width || sz.height() != _resultImage->height)
    {
        //the initial mask was set. the mask was not resized to the providers size
        return;
    }

    QImage mask = _maskEditor->maskImage();
    sz = mask.size();
    ARMARX_CHECK_EQUAL(sz.width(), _resultImage->width);
    ARMARX_CHECK_EQUAL(sz.height(), _resultImage->height);

    //the qimage has an alpha channel!
    for (int x = 0; x < _resultImage->width; ++x)
    {
        for (int y = 0; y < _resultImage->height; ++y)
        {
            int offset = (x + y * _resultImage->width) * 3;
            const QColor color
            {
                mask.pixel(x, y)
            };
            _resultImage->pixels[offset + 0] = color.red();
            _resultImage->pixels[offset + 1] = color.green();
            _resultImage->pixels[offset + 2] = color.blue();
        }
    }
    _maskSet = true;
}



