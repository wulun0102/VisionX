#pragma once

#include <QDialog>

#include <ArmarXCore/core/ManagedIceObject.h>

#include "UserAssistedSegmenterGuiWidgetController.h"


namespace Ui
{
    class UserAssistedSegmenterConfigDialog;
}

namespace visionx
{

    class UserAssistedSegmenterConfigDialog :
        public QDialog, virtual public armarx::ManagedIceObject
    {
        Q_OBJECT

    public:

        explicit UserAssistedSegmenterConfigDialog(QWidget* parent = nullptr);

        virtual ~UserAssistedSegmenterConfigDialog() override;

        virtual std::string getDefaultName() const override
        {
            return "UserAssistedSegmenterConfigDialog" + uuid;
        }

        virtual void onInitComponent() override;
        virtual void onConnectComponent() override;
        virtual void onExitComponent() override;

        std::string getUserAssistedSegmenterProxyName() const;
        std::string getUserAssistedSegmenterTopicName() const;

    signals:


    public slots:

        void verifyConfig();


    private:

        Ui::UserAssistedSegmenterConfigDialog* ui;
        std::string uuid;

        friend class UserAssistedSegmenterGuiWidgetController;

    };

}
