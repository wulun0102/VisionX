#include <EGL/egl.h>

#include <iostream>

int main(int argc, char* argv[])
{
    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (display == EGL_NO_DISPLAY)
    {
        std::cerr << "eglGetDisplay returned EGL_NO_DISPLAY" << std::endl;
        return -1;
    }

    EGLint major = 0;
    EGLint minor = 0;
    EGLBoolean initOk = eglInitialize(display, &major, &minor);
    if (initOk == EGL_FALSE)
    {
        EGLint error = eglGetError();
        std::cerr << "eglInitialize failed with error " << error
                  << "(hex: " << std::hex << error << ")\n";
        return -2;
    }

    std::cout << "EGL initialized (version: " << major << "." << minor << ")" << std::endl;

    eglTerminate(display);
    return 0;
}
