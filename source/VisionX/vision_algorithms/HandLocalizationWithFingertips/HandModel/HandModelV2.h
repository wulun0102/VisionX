/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>

// IVT
#include "Math/Math3d.h"
#include "Calibration/StereoCalibration.h"
#include "Calibration/Calibration.h"


#include "../ParticleFilter/Polygon.h"



namespace visionx
{
    class CHandModelV2
    {
    public:
        class CFinger
        {
        public:
            CFinger(std::vector<float> aJointOffsets, std::vector<Vec3d> aFingerTipCorners);

            void UpdateJointAngles(std::vector<float> aNewJointValues);

            std::vector<Vec3d> m_aFingerTipCornersInFingerBaseCS; // positions of the fingertip corners in the coordinate system of the finger base
            std::vector<Vec3d> m_aFingerJointsInFingerBaseCS; // positions of the finger joints in the coordinate system of the finger base

        private:
            std::vector<float> m_aJointOffsets; // distance from one joint to the next
            std::vector<float> m_aJointAngles;
            std::vector<Vec3d> m_aFingerTipCornersInLocalCS; // relative positions of the fingertip corners in the coordinate system after the last joint
        };


        CHandModelV2(std::string sConfigFileName, CStereoCalibration* pStereoCalibration);
        virtual ~CHandModelV2() {}
        virtual void UpdateHand(double* pConfig);
        void UpdateHand(Vec3d vHandPosition, Mat3d mHandOrientation, float fPalmJointAngle, std::vector<std::vector<float> >& aFingerJointAngles);

        std::vector<std::vector<Vec3d> > m_aFingerJointsInWorldCS;
        std::vector<std::vector<Vec3d> > m_aFingerTipCornersInWorldCS;

        Vec3d m_vHandPosition;
        Mat3d m_mHandOrientation;

        std::vector<ConvexPolygonCalculations::Polygon> m_aFingerTipPolygonsLeftCam, m_aFingerTipPolygonsRightCam;

        Vec2d m_vTrackingBallPosLeftCam, m_vTrackingBallPosRightCam;
        float m_fTrackingBallRadiusLeftCam, m_fTrackingBallRadiusRightCam;

    protected:
        CHandModelV2() { } // for derived classes

        std::string GetNextNonCommentLine(std::ifstream& sFileStream);

        void CalcProjectedPolygons();

        CStereoCalibration* m_pStereoCalibration;

        std::vector<CFinger*> m_aFingers; // 0th finger is thumb
        std::vector<Vec3d> m_aOffsetsToFingers; // 0: from hand base to first thumb joint, rest: from palm joint to first finger joint
        float m_fPalmJointAngle;

        Vec3d m_vTrackingBallOffset;
        float m_fTrackingBallRadius;
    };
}

