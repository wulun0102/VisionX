#include "ice_point_operators.h"


namespace visionx
{

    bool eq(float lhs, float rhs, float tol = 1e-6f)
    {
        return std::abs(lhs - rhs) < tol;
    }

    bool operator!=(const Point3D& lhs, const Point3D& rhs)
    {
        return !eq(lhs.x, rhs.x) || !eq(lhs.y, rhs.y) || !eq(lhs.z, rhs.z);
    }

    std::ostream& operator<<(std::ostream& os, const Point3D& rhs)
    {
        return os << "[" << rhs.x << " " << rhs.y << " " << rhs.z << "]";
    }


    std::ostream& operator<<(std::ostream& os, const RGBA& rhs)
    {
        return os << "[" << rhs.r << " " << rhs.g << " " << rhs.b << " " << rhs.a << "]";
    }


    bool operator!=(const ColoredPoint3D& lhs, const ColoredPoint3D& rhs)
    {
        return lhs.point != rhs.point || lhs.color != rhs.color;
    }

    std::ostream& operator<<(std::ostream& os, const ColoredPoint3D& rhs)
    {
        return os << "(" << rhs.point << " " << rhs.color << ")";
    }


    bool operator!=(const LabeledPoint3D& lhs, const LabeledPoint3D& rhs)
    {
        return lhs.point != rhs.point || lhs.label != rhs.label;
    }

    std::ostream& operator<<(std::ostream& os, const LabeledPoint3D& rhs)
    {
        return os << "(" << rhs.point << " " << rhs.label << ")";
    }
}
