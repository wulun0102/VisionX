#/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ArMemImageProvider.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <Calibration/Calibration.h>
#include <ArmarXCore/core/exceptions/user/NotImplementedYetException.h>

namespace visionx
{

    armarx::PropertyDefinitionsPtr ArMemImageProvider::createPropertyDefinitions()
    {

        armarx::PropertyDefinitionsPtr defs{new armarx::ComponentPropertyDefinitions{getConfigIdentifier()}};

        defs->required(segment_name, "SegmentName");
        defs->optional(local_memory_hostname, "LocalMemoryHostname", "Leave empty if you wannt to use the local memory of this machine");
        defs->component(memory_resolver, "GlobalMemoryResolver");
        return defs;
    }


    ArMemImageProvider::ArMemImageProvider()
    {

    }


    std::string ArMemImageProvider::getDefaultName() const
    {
        return "ArMemImageProvider";
    }

    void ArMemImageProvider::onInitCapturingImageProvider()
    {
        ARMARX_DEBUG << "Initializing ArMemImageProvider...";

        setNumberImages(1);
        this->frameRate =  1;

        // sample image container to get dimensions
        ImageProcessorToArMemImageContainer image_container;

        ARMARX_INFO << "ArMem Image Size: " << image_container.image->width << "x" << image_container.image->height;
        setImageFormat(ImageDimension(image_container.image->width, image_container.image->height), visionx::eRgb, visionx::eBayerPatternGr);
        setImageSyncMode(visionx::eFpsSynchronization);
        ARMARX_DEBUG << "Finished Initializing ArMemImageProvider...";
    }

    void ArMemImageProvider::onExitCapturingImageProvider()
    {
    }

    bool ArMemImageProvider::capture(void** ppImageBuffers)
    {
        ARMARX_DEBUG << "Running capture of ArMemImageProvider";

        TIMING_START(GetLatestCommit);
        armarx::armem::ArMemCommitPtr commit = local_memory->getLatestCommitFromSegment(segment_name);
        TIMING_END(GetLatestCommit);
        if (!commit) // no data in memory yet
        {
            return false;
        }

        std::vector<armarx::aron::data::AronDataPtr> aron_data = commit->data;

        ARMARX_DEBUG << "The aron commit contains " << aron_data.size() << " elements!";

        armarx::SharedMemoryScopedWriteLockPtr lock(sharedMemoryProvider->getScopedWriteLock());

        ImageProcessorToArMemImageContainer image_container;
        TIMING_START(FromAron);
        image_container.fromAron(aron_data[0]);
        TIMING_END(FromAron);

        ARMARX_DEBUG << "Transformed aron back to container";

        memcpy(ppImageBuffers[0], reinterpret_cast<unsigned char*>(image_container.image->pixels), image_container.image->width * image_container.image->height * image_container.image->bytesPerPixel);

        ARMARX_DEBUG << "Finished Running capture of ArMemImageProvider";
        return true;
    }

    void ArMemImageProvider::onStartCapture(float framesPerSecond)
    {
        ARMARX_INFO << "Start capturing ArMemImageProvider";

        // Get Local Memory
        if (local_memory_hostname == "")
        {
            local_memory_hostname = memory_resolver->getHostnameOfCurrentMachine();
        }
        local_memory = memory_resolver->getMemoryForHostname(local_memory_hostname);
        ARMARX_INFO << "Finished start capturing ArMemImageProvider";
    }

    void ArMemImageProvider::onStopCapture()
    {
    }


    MonocularCalibration ArMemImageProvider::getCalibration(const Ice::Current&)
    {
        throw armarx::exceptions::user::NotImplementedYetException("Calibration NYI");
    }
}
