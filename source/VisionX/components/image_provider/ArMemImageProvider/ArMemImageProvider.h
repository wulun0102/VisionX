/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/components/Calibration.h>
#include <opencv2/highgui/highgui.hpp>


// Aron/ArMem Stuff
#include <RobotAPI/interface/armem.h>
#include <RobotAPI/interface/aron.h> // not necessary

// Generated
#include <VisionX/components/image_processor/ImageProcessorToArMem/aron/ImageProcessorToArMemImage.aron.generated.h>


namespace visionx
{
    class ArMemImageProvider :
        public CapturingImageProvider
    {
    public:
        ArMemImageProvider();

    protected:
        std::string getDefaultName() const override;

    protected:
        void onInitCapturingImageProvider() override;
        void onExitCapturingImageProvider() override;
        bool capture(void** ppImageBuffers) override;

    protected:
        void onStartCapture(float framesPerSecond) override;
        void onStopCapture() override;

    public:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    public:
        MonocularCalibration getCalibration(const Ice::Current&);
    protected:
        MonocularCalibration calibration;

    private:
        // properties
        std::string segment_name = "";
        armarx::armem::ArMemGlobalMemoryResolverPrx memory_resolver;
        std::string local_memory_hostname = "";
        armarx::armem::ArMemLocalMemoryInterfacePrx local_memory;
    };

}

