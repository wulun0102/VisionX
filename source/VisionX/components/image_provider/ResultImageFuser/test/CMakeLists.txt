
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ResultImageFuser)
 
armarx_add_test(ResultImageFuserTest ResultImageFuserTest.cpp "${LIBS}")