armarx_component_set_name("WebCamImageProvider")
set(COMPONENT_LIBS VisionXCore)
set(SOURCES WebCamImageProvider.cpp)
set(HEADERS WebCamImageProvider.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

armarx_generate_and_add_component_executable(COMPONENT_NAMESPACE visionx APPLICATION_APP_SUFFIX)
