
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore UCLObjectRecognition)
 
armarx_add_test(UCLObjectRecognitionTest UCLObjectRecognitionTest.cpp "${LIBS}")