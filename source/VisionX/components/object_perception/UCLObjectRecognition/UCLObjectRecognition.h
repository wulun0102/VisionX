/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::UCLObjectRecognition
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/speech/SpeechInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <MemoryX/interface/components/LongtermMemoryInterface.h>

#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>
#include <VisionX/interface/components/FaceRecognitionInterface.h>
#include <VisionX/interface/components/ObjectPoseInterface.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>
#include <VisionX/tools/TypeMapping.h>

//#include <Python.h>


namespace visionx
{
    /**
     * @class UCLObjectRecognitionPropertyDefinitions
     * @brief
     */
    class UCLObjectRecognitionPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        UCLObjectRecognitionPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("AgentName", "Armar6", "Name of the robot that does the localization");
            defineOptionalProperty<std::string>("CameraFrameName", "DepthCamera", "Name of the camera frame of the robot that does the localization");
            defineOptionalProperty<std::string>("ObjectNameIdMap", "spraybottle:5;brush:1", "map between object names and mask rcnn names");

            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic");
        }
    };

    /**
     * @defgroup Component-UCLObjectRecognition UCLObjectRecognition
     * @ingroup VisionX-Components
     * A description of the component UCLObjectRecognition.
     *
     * @class UCLObjectRecognition
     * @ingroup Component-UCLObjectRecognition
     * @brief Brief description of class UCLObjectRecognition.
     *
     * Detailed description of class UCLObjectRecognition.
     */
    class UCLObjectRecognition :
        public armarx::Component,
        virtual public memoryx::ObjectLocalizerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "UCLObjectRecognition";
        }

    protected:
        memoryx::ObjectLocalizationResultList localizeAndIdentifyFaces(CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages);
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        typedef std::map<std::string, std::list<std::pair<IceUtil::Time, double> > > FaceConfidenceHistory;
    private:

        ObjectPoseInterfacePrx prx;
        armarx::DebugDrawerInterfacePrx debugDrawerTopicPrx;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;
    public:
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& classes, const Ice::Current&) override;

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;

    };
}

