/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <iostream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>


class RGSegClass
{
public:
    RGSegClass();

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& ApplyRegionGrowingSegmentation(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr);

    void UpdateRegionGrowingSegmentationParameters(float SmoothnessThres, float CurvatureThres);


private:

    using PointT = pcl::PointXYZRGBA;  // The point type used for input
    using PointL = pcl::PointXYZL;  // The point type used for input

    pcl::PointCloud<PointT>::Ptr input_cloud_ptr;
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr output_colored_cloud;
    pcl::PointCloud<PointL>::Ptr output_labeled_cloud;

    float SmoothnessThreshold;
    float CurvatureThreshold;

    void RegionGrowingSegmenter();
    void ConvertFromXYZRGBAtoXYZL();


};

