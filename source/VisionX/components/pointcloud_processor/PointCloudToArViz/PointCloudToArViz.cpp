/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudToArViz.h"

#include <pcl/point_types.h>
#include <pcl/common/transforms.h>

#include <RobotAPI/libraries/core/Pose.h>


namespace visionx
{

    PointCloudToArVizPropertyDefinitions::PointCloudToArVizPropertyDefinitions(std::string prefix) :
        visionx::PointCloudProcessorPropertyDefinitions(prefix)
    {
        defineOptionalProperty<int>("ProviderWaitMs", 100,
                                    "Time to wait for each point cloud provider [ms].");
    }

    armarx::PropertyDefinitionsPtr PointCloudToArViz::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new PointCloudToArVizPropertyDefinitions(getConfigIdentifier()));

        defs->optional(params.pointSizeInPixels, "viz.pointSizeInPixels", "The point size in pixels.");
        defs->optional(params.checkFinite, "viz.checkFinite",
                       "Enable to check points for being finite. "
                       "\nEnable this option if the drawn point cloud causes the screen to become purely white.");
        defs->optional(params.labeled, "viz.labeled",
                       "If true and point cloud is labeled, draw colors according to labels.");

        return defs;
    }



    std::string PointCloudToArViz::getDefaultName() const
    {
        return "PointCloudToArViz";
    }


    void PointCloudToArViz::onInitPointCloudProcessor()
    {
        params.providerWaitTime = IceUtil::Time::milliSeconds(getProperty<int>("ProviderWaitMs").getValue());
    }


    void PointCloudToArViz::onConnectPointCloudProcessor()
    {
        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void PointCloudToArViz::onDisconnectPointCloudProcessor()
    {
    }

    void PointCloudToArViz::onExitPointCloudProcessor()
    {
    }


    void PointCloudToArViz::process()
    {
        // Fetch input clouds.
        for (const std::string& providerName : getPointCloudProviderNames())
        {
            if (waitForPointClouds(providerName, static_cast<int>(params.providerWaitTime.toMilliSeconds())))
            {
                auto it = cache.find(providerName);
                if (it == cache.end())
                {
                    it = cache.emplace(providerName, armarx::viz::PointCloud(providerName)).first;
                }
                armarx::viz::PointCloud& vizCloud = it->second;

                Parameters params;
                {
                    std::scoped_lock lock(paramMutex);
                    params = this->params;
                }

                vizCloud.checkFinite(params.checkFinite);
                vizCloud.pointSizeInPixels(params.pointSizeInPixels);

                const PointContentType type = getPointCloudFormat(providerName)->type;
                if (visionx::tools::isLabeled(type))
                {
                    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inputCloud(new pcl::PointCloud<pcl::PointXYZRGBL>);
                    getPointClouds(providerName, inputCloud);
                    params.applyCustomTransform(*inputCloud);
                    vizCloud.pointCloud(*inputCloud, params.labeled);
                }
                else
                {
                    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr inputCloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
                    getPointClouds(providerName, inputCloud);
                    params.applyCustomTransform(*inputCloud);
                    vizCloud.pointCloud(*inputCloud);
                }

                armarx::viz::Layer layer = arviz.layer(providerName);
                layer.add(vizCloud);
                arviz.commit(layer);
            }
            else
            {
                ARMARX_VERBOSE << "Timeout waiting for point cloud provider " << providerName << ".";
            }
        }
    }

    void PointCloudToArViz::setPointSizeInPixels(float pointSizeInPixels, const Ice::Current&)
    {
        std::scoped_lock lock(paramMutex);
        params.pointSizeInPixels = pointSizeInPixels;
    }

    void PointCloudToArViz::setCustomTransform(const armarx::PoseBasePtr& transformPtr, const Ice::Current&)
    {
        std::scoped_lock lock(paramMutex);
        params.customTransform = armarx::PosePtr::dynamicCast(transformPtr)->toEigen();
        if (params.customTransform->isIdentity())
        {
            params.customTransform = std::nullopt;
        }
    }

    template<class PointT>
    void PointCloudToArViz::Parameters::applyCustomTransform(pcl::PointCloud<PointT>& pointCloud)
    {
        if (customTransform)
        {
            pcl::transformPointCloud(pointCloud, pointCloud, *customTransform);
        }
    }


    void PointCloudToArViz::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        GridLayout grid;
        int row = 0;

        tab.checkFinite.setName("Check Finite");
        tab.checkFinite.setValue(params.checkFinite);

        tab.pointSizeInPixels.setName("Point Size [pixels]");
        tab.pointSizeInPixels.setDecimals(2);
        tab.pointSizeInPixels.setRange(0, 100);
        tab.pointSizeInPixels.setSteps(100);
        tab.pointSizeInPixels.setValue(params.pointSizeInPixels);

        tab.labeled.setName("Color by Labels");
        tab.labeled.setValue(params.labeled);

        grid.add(Label(tab.checkFinite.getName()), {row, 0}).add(tab.checkFinite, {row, 1});
        row++;
        grid.add(Label(tab.pointSizeInPixels.getName()), {row, 0}).add(tab.pointSizeInPixels, {row, 1});
        row++;
        grid.add(Label(tab.labeled.getName()), {row, 0}).add(tab.labeled, {row, 1});
        row++;

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }

    void PointCloudToArViz::RemoteGui_update()
    {
        std::scoped_lock lock(paramMutex);
        if (tab.checkFinite.hasValueChanged())
        {
            params.checkFinite = tab.checkFinite.getValue();
        }
        if (tab.pointSizeInPixels.hasValueChanged())
        {
            params.pointSizeInPixels = tab.pointSizeInPixels.getValue();
        }
        if (tab.labeled.hasValueChanged())
        {
            params.labeled = tab.labeled.getValue();
        }
    }


}
