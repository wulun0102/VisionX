/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>


#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <RobotAPI/libraries/core/FramedPose.h>


#include <MemoryX/interface/memorytypes/MemoryEntities.h>

namespace armarx
{
    /**
     * @class PrimitiveFilter
     * @brief A brief description
     *
     * Detailed Description
     */
    class PrimitiveFilter
    {
    public:
        /**
         * PrimitiveFilter Constructor
         */
        PrimitiveFilter();

        /**
         * PrimitiveFilter Destructor
         */
        ~PrimitiveFilter();

        Eigen::Matrix3Xf updateCameraPosition(Eigen::Matrix4f cameraToWorld);

        const Eigen::Matrix3f getFrustum();

        bool intersectsFrustum(Eigen::Matrix4f pose, Eigen::Vector3f extent);

        bool isPointInFrustum(Eigen::Vector3f point);

        void getPrimitivesInFieldOfView(std::vector<memoryx::EnvironmentalPrimitiveBasePtr> allPrimitives,  std::vector<memoryx::EnvironmentalPrimitiveBasePtr>& visiblePrimitives, std::vector<memoryx::EnvironmentalPrimitiveBasePtr>& otherPrimitives);

        void visualizeFrustum(DebugDrawerInterfacePrx debugDrawerTopicPrx);

    private:

        Eigen::Matrix<float, 3, 18> frustum;

    };
}
