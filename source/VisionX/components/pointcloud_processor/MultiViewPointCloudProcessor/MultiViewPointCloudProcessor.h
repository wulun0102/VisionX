/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::MultiViewPointCloudProcessor
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <any>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/PluginAll.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <VisionX/interface/components/MultiViewPointCloudProcessor.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>


ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
    armarx::cfg::MVPCLProc, Config,
    (
        float, gridSize, AX_DEFAULT(50),  AX_MIN(0), AX_MAX(10'000),
        AX_DESCRIPTION("Grid size for approximate voxel grid (0 = disabled)")
    ),
    (float, frameRate, AX_DEFAULT(30),  AX_MIN(0), AX_MAX(300)),
    (int, maxNumberOfFramesPerViewPoint, AX_DEFAULT(100),  AX_MIN(1), AX_MAX(1000))
);

namespace armarx
{
    /**
     * @defgroup Component-MultiViewPointCloudProcessor MultiViewPointCloudProcessor
     * @ingroup VisionX-Components
     * A description of the component MultiViewPointCloudProcessor.
     *
     * @class MultiViewPointCloudProcessor
     * @ingroup Component-MultiViewPointCloudProcessor
     * @brief Brief description of class MultiViewPointCloudProcessor.
     *
     * Detailed description of class MultiViewPointCloudProcessor.
     */
    class MultiViewPointCloudProcessor :
        virtual public visionx::MultiViewPointCloudProcessorInterface,
        virtual public visionx::PointCloudProcessor,
        virtual public armarx::RobotStateComponentPluginUser,
        virtual public DebugObserverComponentPluginUser,
        virtual public RemoteGuiComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        //ice interface
        void startRecordingNextViewPoint(const Ice::Current& = Ice::emptyCurrent) override;
        void stopRecordingViewPoint(const Ice::Current& = Ice::emptyCurrent) override;
        void clearViewPoints(const Ice::Current& = Ice::emptyCurrent) override;
        void setMaxNumberOfFramesPerViewPoint(Ice::Int n, const Ice::Current& = Ice::emptyCurrent) override;
        void setDownsamplingGridSize(Ice::Float s, const Ice::Current& = Ice::emptyCurrent) override;
        void setFrameRate(Ice::Float r, const Ice::Current& = Ice::emptyCurrent) override;

    protected:
        RemoteGui::WidgetPtr buildGui();
        void processGui(RemoteGui::TabProxy& prx);

        void process() override;
        template<class PointType> void process();
        template<class PointType> void doProvide();

        void onInitPointCloudProcessor() override;
        void onConnectPointCloudProcessor() override;
        void onDisconnectPointCloudProcessor() override {}
        void onExitPointCloudProcessor() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    private:
        //provider data
        std::string                                 _pointCloudProviderName;
        visionx::PointCloudProviderInfo             _pointCloudProviderInfo;
        visionx::PointCloudProviderInterfacePrx     _pointCloudProvider;
        std::string                                 _pointCloudProviderRefFrame;

        mutable std::recursive_mutex                      _cfgBufWriteMutex;
        mutable std::recursive_mutex                      _cfgBufReadMutex;
        WriteBufferedTripleBuffer<cfg::MVPCLProc::Config> _cfgBuf;


        VirtualRobot::RobotPtr _robot;
        struct ViewPoint
        {
            std::deque<std::any> clouds;
        };
        mutable std::recursive_mutex    _viewPointsMutex;
        std::deque<ViewPoint>           _viewPoints;
        std::atomic_int     _currentViewpoint = -1;
        std::atomic_bool    _doRecord = false;
        std::atomic_bool    _doClear  = false;

        std::thread         _providerThread;
        std::atomic_bool    _stopProviding = false;
    };
}
