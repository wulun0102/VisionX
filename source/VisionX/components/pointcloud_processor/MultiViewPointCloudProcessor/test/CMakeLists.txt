
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore MultiViewPointCloudProcessor)
 
armarx_add_test(MultiViewPointCloudProcessorTest MultiViewPointCloudProcessorTest.cpp "${LIBS}")
