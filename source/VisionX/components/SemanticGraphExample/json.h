#pragma once

#include <SemanticObjectRelations/Serialization/json.h>

#include "MyGraph.h"


namespace mygraph
{
    void to_json(nlohmann::json& j, const MyVertex& vertex);
    void from_json(const nlohmann::json& j, MyVertex& vertex);

    void to_json(nlohmann::json& j, const MyEdge& edge);
    void from_json(const nlohmann::json& j, MyEdge& edge);

    void to_json(nlohmann::json& j, const MyGraphAttributes& graph);
    void from_json(const nlohmann::json& j, MyGraphAttributes& graph);
}
