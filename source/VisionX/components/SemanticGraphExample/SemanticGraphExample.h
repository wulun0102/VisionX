/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SemanticGraphExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>


namespace armarx
{
    /**
     * @class SemanticGraphExamplePropertyDefinitions
     * @brief Property definitions of `SemanticGraphExample`.
     */
    class SemanticGraphExamplePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SemanticGraphExamplePropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-SemanticGraphExample SemanticGraphExample
     * @ingroup VisionX-Components
     * A description of the component SemanticGraphExample.
     *
     * @class SemanticGraphExample
     * @ingroup Component-SemanticGraphExample
     * @brief Brief description of class SemanticGraphExample.
     *
     * Detailed description of class SemanticGraphExample.
     */
    class SemanticGraphExample :
        virtual public armarx::Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        armarx::semantic::GraphStorageTopicPrx graphTopic;

    };
}
