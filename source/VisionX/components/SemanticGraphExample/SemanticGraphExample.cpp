/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SemanticGraphExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <VirtualRobot/VirtualRobot.h>

#include "SemanticGraphExample.h"

#include <VisionX/libraries/SemanticObjectRelations/ice_serialization.h>

#include "MyGraph.h"
#include "json.h"


namespace armarx
{

    SemanticGraphExamplePropertyDefinitions::SemanticGraphExamplePropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr SemanticGraphExample::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new SemanticGraphExamplePropertyDefinitions(getConfigIdentifier());

        defs->topic(graphTopic, "SemanticGraphTopic");

        return defs;
    }

    std::string SemanticGraphExample::getDefaultName() const
    {
        return "SemanticGraphExample";
    }


    void SemanticGraphExample::onInitComponent()
    {
    }



    void SemanticGraphExample::onConnectComponent()
    {
        mygraph::MyGraph graph;

        // Add vertices and set attributes.

        // Option 1: Prepare vertex attributes beforehand.
        mygraph::MyVertex vattribs;
        vattribs.name = "A";
        vattribs.value = 0.0;
        auto a = graph.addVertex(semrel::ShapeID{0}, vattribs);

        vattribs.name = "B";
        vattribs.value = 0.5;
        auto b = graph.addVertex(semrel::ShapeID{1}, vattribs);

        // Option 1: Set attributes after adding.
        auto c = graph.addVertex(semrel::ShapeID{5});
        c.attrib().name = "C";
        c.attrib().value = 1.0;

        // Add edges and set attributes.
        auto ab = graph.addEdge(a, b);
        ab.attrib().value = 0;
        auto ac = graph.addEdge(a, c);
        ac.attrib().value = 1;
        auto bc = graph.addEdge(b, c);
        bc.attrib().value = 2;

        // Set graph attributes
        graph.attrib().position = {1, 2, 3};

        graphTopic->reportGraph("MyGraph", armarx::semantic::toIce(graph));
    }


    void SemanticGraphExample::onDisconnectComponent()
    {
    }


    void SemanticGraphExample::onExitComponent()
    {
    }

}
