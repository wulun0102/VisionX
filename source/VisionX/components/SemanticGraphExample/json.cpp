#include "json.h"

#include <SimoxUtility/color/cmaps.h>
#include <SimoxUtility/color/interpolation.h>


void mygraph::to_json(nlohmann::json& j, const mygraph::MyVertex& vertex)
{
    static simox::ColorMap cmap = simox::color::cmaps::viridis();

    j["name"] = vertex.name;
    j["value"] = vertex.value;

    j["style"]["label"] = vertex.name;
    simox::Color color = simox::color::interpol::linear(0.5, cmap(vertex.value), simox::Color::white());
    j["style"]["fill-color"] = color.to_vector4i();
}

void mygraph::from_json(const nlohmann::json& j, mygraph::MyVertex& vertex)
{
    j.at("name").get_to(vertex.name);
    j.at("value").get_to(vertex.value);

}

void mygraph::to_json(nlohmann::json& j, const mygraph::MyEdge& edge)
{
    j["value"] = edge.value;

    simox::Color color;
    switch (edge.value)
    {
        case 1:
            color = simox::Color::blue();
            break;
        case 2:
            color = simox::Color::green();
            break;
        default:
            color = simox::Color::black();
            break;
    }

    j["style"]["color"] = color.to_vector4i();
    if (edge.value > 0)
    {
        j["style"]["label"] = std::to_string(edge.value);
    }
}

void mygraph::from_json(const nlohmann::json& j, mygraph::MyEdge& edge)
{
    j.at("value").get_to(edge.value);
}

void mygraph::to_json(nlohmann::json& j, const mygraph::MyGraphAttributes& graph)
{
    j["position"] = graph.position;
    j["orientation"] = graph.orientation;
}

void mygraph::from_json(const nlohmann::json& j, mygraph::MyGraphAttributes& graph)
{
    j.at("position").get_to(graph.position);
    j.at("orientation").get_to(graph.orientation);
}
