#pragma once

#include <SemanticObjectRelations/RelationGraph.h>


namespace mygraph
{
    struct MyVertex : public semrel::ShapeVertex
    {
        std::string name;
        float value = 0;
    };

    struct MyEdge
    {
        int value = 1;
    };

    struct MyGraphAttributes
    {
        Eigen::Vector3f position = Eigen::Vector3f::Zero();
        Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();
    };

    using MyGraph = semrel::RelationGraph<MyVertex, MyEdge, MyGraphAttributes>;

}
