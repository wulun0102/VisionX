/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RecordingManager
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <atomic>
#include <map>
#include <mutex>
#include <optional>
#include <shared_mutex>
#include <string>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/components/ImageRecordingManager/ImageRecordingManagerInterface.h>


namespace visionx::imrecman
{

    class Component:
        virtual public armarx::Component,
        virtual public visionx::ImageRecordingManagerInterface
    {

    private:

        std::shared_mutex m_configs_mx;
        std::string m_configs_json;
        std::optional<std::map<std::string, imrec::Config>> m_configs;

    public:

        ~Component() override;

        void onInitComponent() override;

        void onConnectComponent() override;

        void onDisconnectComponent() override;

        void onExitComponent() override;

        static std::string GetDefaultName();

        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void configureRecordings(const std::map<std::string, imrec::Config>&, const Ice::Current&) override;

        void configureRecordingsFromJson(const std::string&, const Ice::Current&) override;

        std::map<std::string, imrec::Config> getConfiguration(const Ice::Current&) override;

        bool isConfigured(const Ice::Current&) override;

        std::map<std::string, visionx::imrecman::StartStopStatus> startRecordings(const Ice::Current&) override;

        std::map<std::string, visionx::imrec::Status> getRecordingStatuses(const Ice::Current&) override;

        std::map<std::string, visionx::imrecman::StartStopStatus> stopRecordings(const Ice::Current&) override;

    };

}


namespace visionx
{
    using ImageRecordingManager = imrecman::Component;
}
