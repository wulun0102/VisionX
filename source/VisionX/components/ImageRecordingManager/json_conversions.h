/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RecordingManager
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Simox
#include <SimoxUtility/json.h>

// ArmarX
#include <ArmarXCore/core/json_conversions.h>
#include <VisionX/components/ImageRecordingManager/ImageRecordingManagerInterface.h>


namespace visionx::imrec
{
    void to_json(nlohmann::json& j, const ChannelConfig& cc)
    {
        j = nlohmann::json
        {
            {"disabled", cc.disabled},
            {"name", cc.name},
            {"format", cc.format},
            {"fps", cc.fps}
        };
    }

    void from_json(const nlohmann::json& j, ChannelConfig& cc)
    {
        j.at("disabled").get_to(cc.disabled);
        j.at("name").get_to(cc.name);
        j.at("format").get_to(cc.format);
        j.at("fps").get_to(cc.fps);
    }

    void to_json(nlohmann::json& j, const Config& c)
    {
        j = nlohmann::json
        {
            {"name", c.name},
            {"location", c.location},
            {"startTimestamp", c.startTimestamp},
            {"channelConfigs", c.channelConfigs}
        };
    }

    void from_json(const nlohmann::json& j, Config& c)
    {
        j.at("name").get_to(c.name);
        j.at("location").get_to(c.location);
        j.at("startTimestamp").get_to(c.startTimestamp);
        j.at("channelConfigs").get_to(c.channelConfigs);
    }
}
