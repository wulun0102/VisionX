/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>

// STD/STL
#include <memory>
#include <mutex>
#include <string>
#include <vector>

// IVT
#include <Image/ByteImage.h>

// Ice
#include <Ice/Current.h>
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/interface/core/ImageProviderInterface.h>

// Aron/ArMem Stuff
#include <RobotAPI/interface/armem.h>
#include <RobotAPI/interface/aron.h> // not necessary

// Generated
#include "aron/ImageProcessorToArMemImage.aron.generated.h"

namespace visionx
{
    class ImageProcessorToArMem :
        public visionx::ImageProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ImageProcessorToArMem";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        // ImageProcessor interface
        void onInitImageProcessor() override;
        void onConnectImageProcessor()  override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

    private:
        void checkForNewImages();

        // Meta (Properties)
        std::string m_image_provider_id;
        unsigned int m_image_provider_channel;
        visionx::ImageProviderInterface::ProxyType m_image_provider;
        visionx::ImageProviderInfo m_image_provider_info;
        armarx::MetaInfoSizeBase::PointerType m_image_meta_info;
        IceUtil::Time m_timestamp_last_image;

        // ImageBuffer und ImageInformations
        CByteImage** m_input_image_buf;
        visionx::ImageProcessorToArMemImageContainer m_aron_input_image_container; // the image dimensions should be correct already!
        std::mutex m_input_image_mutex;

        unsigned int num_of_received_images;

        // Threads and program flow information
        const unsigned int m_periodic_task_interval = 100;
        armarx::PeriodicTask<visionx::ImageProcessorToArMem>::pointer_type m_periodic_task;
        std::mutex m_input_image_received_mutex;
        bool m_image_received; // Is true, if new images are available (when mode is 'FromTopic' this also means that corresponding keypoints are available)

        // properties
        armarx::armem::ArMemGlobalMemoryResolverPrx memory_resolver;
        std::string local_memory_hostname = "";
        armarx::armem::ArMemLocalMemoryInterfacePrx local_memory;

    };
}
