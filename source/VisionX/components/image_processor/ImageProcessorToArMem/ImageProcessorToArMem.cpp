/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageProcessorToArMem.h"


// STD/STL

// IVT
#include <Image/ImageProcessor.h>

using namespace visionx;


armarx::PropertyDefinitionsPtr ImageProcessorToArMem::createPropertyDefinitions()
{
    armarx::PropertyDefinitionsPtr defs{new armarx::ComponentPropertyDefinitions{getConfigIdentifier()}};

    defs->optional(local_memory_hostname, "Local Memory Hostname", "Leave empty if you wannt to use the local memory of this machine");
    defs->optional(m_image_provider_id, "PrividerID");
    defs->optional(m_image_provider_channel, "Channel");
    defs->component(memory_resolver, "GlobalMemoryResolver");
    return defs;
}

void visionx::ImageProcessorToArMem::onInitImageProcessor()
{
    ARMARX_DEBUG << "Initializing SimpleEpisodicMemoryImageConnector";

    ARMARX_VERBOSE << "Using image provider with ID '" << m_image_provider_id << "' and channel " << m_image_provider_channel << ".";

    usingImageProvider(m_image_provider_id);
    m_image_received = false;
    setFramerate(1);
}

void visionx::ImageProcessorToArMem::onConnectImageProcessor()
{
    // Get Local Memory
    if (local_memory_hostname == "")
    {
        local_memory_hostname = memory_resolver->getHostnameOfCurrentMachine();
    }
    local_memory = memory_resolver->getMemoryForHostname(local_memory_hostname);


    // Connect to image provider.
    m_image_provider_info = getImageProvider(m_image_provider_id);
    m_image_provider = getProxy<visionx::ImageProviderInterfacePrx>(m_image_provider_id);

    // Init input image.
    num_of_received_images = static_cast<unsigned int>(m_image_provider_info.numberImages);
    m_input_image_buf = new ::CByteImage*[num_of_received_images];
    for (unsigned int i = 0; i < num_of_received_images; ++i)
    {
        m_input_image_buf[i] = visionx::tools::createByteImage(m_image_provider_info);
    }
    //m_input_image = visionx::tools::createByteImage(m_image_provider_info); old image processor code

    // Kick off running task
    m_periodic_task = new armarx::PeriodicTask<ImageProcessorToArMem>(this, &ImageProcessorToArMem::checkForNewImages, m_periodic_task_interval);
    m_periodic_task->start();
}

void visionx::ImageProcessorToArMem::onDisconnectImageProcessor()
{
    // Stop task.
    {
        m_periodic_task->stop();
    }

    // Clear input image buffer.
    {
        for (unsigned int i = 0; i < num_of_received_images; ++i)
        {
            delete m_input_image_buf[i];
        }
        delete[] m_input_image_buf;
    }
}

void visionx::ImageProcessorToArMem::onExitImageProcessor()
{
}

void visionx::ImageProcessorToArMem::process()
{
    const IceUtil::Time timeout = IceUtil::Time::milliSeconds(1000);
    if (!waitForImages(m_image_provider_id, static_cast<int>(timeout.toMilliSeconds())))
    {
        ARMARX_WARNING << "Timeout while waiting for camera images (>" << timeout << ")";
        return;
    }

    ARMARX_DEBUG << "Received a new image. Putting image to member variable";

    int num_images = getImages(m_image_provider_id, m_input_image_buf, m_image_meta_info);

    if (static_cast<unsigned int>(num_images) != num_of_received_images)
    {
        ARMARX_ERROR << "Received unexpected number of input images. Got " << num_images << " instead of " <<  num_of_received_images;
    }
    else
    {
        // Only consider channel image.
        std::lock_guard<std::mutex> lock{m_input_image_received_mutex};

        if (not(m_aron_input_image_container.image.get()))
        {
            ARMARX_ERROR << "The aron image container contains null. This should not happen, did you manipulate the aron class?";
        }

        ::ImageProcessor::CopyImage(m_input_image_buf[m_image_provider_channel], m_aron_input_image_container.image.get());
        m_aron_input_image_container.image_meta_information = "Hello i am some fancy meta information";

        m_timestamp_last_image = IceUtil::Time::microSeconds(m_image_meta_info->timeProvided);
        m_image_received = true;
    }
    ARMARX_DEBUG << "Wait for next image";
}

void visionx::ImageProcessorToArMem::checkForNewImages()
{
    armarx::aron::data::AronDataPtr aron_data = nullptr; // will be assigned later
    {
        std::lock_guard<std::mutex> received_lock{m_input_image_received_mutex};
        if (!m_image_received)
        {
            return;
        }

        ARMARX_DEBUG << "Got a new image for processing. Send image to ArMemMemory. Image information: Size: " <<
                     m_aron_input_image_container.image->width << ", " << m_aron_input_image_container.image->height << ", " <<
                     m_aron_input_image_container.image->bytesPerPixel;

        TIMING_START(ToAron);
        aron_data = m_aron_input_image_container.toAron(); // In Aron, the data is copied, from now on we can change the image container again
        TIMING_END(ToAron);
        m_image_received = false;
    }
    TIMING_START(Commit);
    local_memory->commit_single("ImageProcessorToArMemSegment", m_timestamp_last_image.toMilliSecondsDouble(), aron_data); // now the data is part of the local memory
    TIMING_END(Commit);

    ARMARX_DEBUG << deactivateSpam(0.5) << "Processed a new image and sent it to the memory";
}
