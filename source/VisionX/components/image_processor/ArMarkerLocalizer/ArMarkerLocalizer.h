/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>
#include <VisionX/interface/components/ArMarkerLocalizerInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// ArUco
#include <aruco/aruco.h>

#include <mutex>


// forward declarations
class CByteImage;

namespace visionx
{
    class ArMarkerLocalizerPropertyDefinitions :
        public ImageProcessorPropertyDefinitions
    {
    public:
        ArMarkerLocalizerPropertyDefinitions(std::string prefix);
    };


    /**
     * ArMarkerLocalizer uses CTexturedRecognition of IVTRecognition in order to recognize and localize objects.
     * The object data is read from PriorKnowledge and CommonStorage via MemoryX.
     * The object localization is invoked automatically by the working memory if the object has been requested there.
     */
    class ArMarkerLocalizer :
        virtual public ArMarkerLocalizerInterface,
        virtual public ImageProcessor,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
    public:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see Component::getDefaultName()
        std::string getDefaultName() const override;


    public:
        // ArMarkerLocalizerInterface interface
        visionx::ArMarkerLocalizationResultList LocalizeAllMarkersNow(const Ice::Current&) override;
        visionx::ArMarkerLocalizationResultList GetLatestLocalizationResult(const Ice::Current&) override;


    protected:

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override {}
        void onExitImageProcessor() override {}


        void process() override;


        void createRemoteGuiTab();
        void RemoteGui_update() override;


    private:


        IceUtil::Time startingTime;
        aruco::CameraParameters arucoCameraParameters;
        aruco::MarkerDetector markerDetector;
        std::atomic<float> markerSize;
        std::map<std::string, int> markerIDs;
        std::map<std::string, float> markerSizes;

        CByteImage** cameraImages;

        std::string imageProviderName;
        bool gotAnyImages = false;

        visionx::ArMarkerLocalizationResultList lastLocalizationResult;
        std::mutex resultMutex;

        visionx::ArMarkerLocalizationResultList localizeAllMarkersInternal();


        bool visuEnabled = false;


        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::FloatSpinBox markerSize;
        };
        RemoteGuiTab tab;

    };

}

