armarx_component_set_name(RobotHandLocalizationWithFingertips)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(LDFLAGS "${LDFLAGS} ${OpenMP_CXX_FLAGS}")

set(COMPONENT_LIBS HandLocalizationWithFingertips
                   VisionXInterfaces
                   VisionXCore
                   VisionXTools
                   ArmarXCore
                   RobotAPICore
                   ArmarXCoreObservers
                   RobotAPIRobotStateComponent
                   gomp
)

set(SOURCES RobotHandLocalizationWithFingertips.cpp)
set(HEADERS RobotHandLocalizationWithFingertips.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

set(EXE_SOURCES main.cpp RobotHandLocalizationWithFingertipsApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
