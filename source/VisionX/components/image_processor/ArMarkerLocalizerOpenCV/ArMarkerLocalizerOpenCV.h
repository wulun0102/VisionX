/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <opencv2/aruco.hpp>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>
#include <VisionX/interface/components/ArMarkerLocalizerInterface.h>


// forward declarations
class CByteImage;

namespace visionx
{
    class ArMarkerLocalizerOpenCVPropertyDefinitions :
        public ImageProcessorPropertyDefinitions
    {
    public:
        ArMarkerLocalizerOpenCVPropertyDefinitions(std::string prefix);
    };


    /**
     * ArMarkerLocalizerOpenCV uses CTexturedRecognition of IVTRecognition in order to recognize and localize objects.
     * The object data is read from PriorKnowledge and CommonStorage via MemoryX.
     * The object localization is invoked automatically by the working memory if the object has been requested there.
     */
    class ArMarkerLocalizerOpenCV :
        virtual public ImageProcessor
        , virtual public ArMarkerLocalizerInterface
        , virtual public armarx::ArVizComponentPluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
    public:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see Component::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override {}
        void onExitImageProcessor() override {}

        void process() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;


    public:
        // ArMarkerLocalizerOpenCVInterface interface
        visionx::ArMarkerLocalizationResultList LocalizeAllMarkersNow(const Ice::Current&) override;
        visionx::ArMarkerLocalizationResultList GetLatestLocalizationResult(const Ice::Current&) override;


    private:

        visionx::ArMarkerLocalizationResultList localizeAllMarkersInternal();


    private:

        struct Properties
        {
            std::atomic<float> markerSize = 40.0;
            int dictionary = cv::aruco::DICT_ARUCO_ORIGINAL;

            std::string referenceFrame = "DepthCamera";
            std::string agentName = "Armar6";
            std::string imageProviderName = "ImageProvider";

            // "Vector of distortion coefficients (k1,k2,p1,p2 [,k3 [,k4,k5,k6], [s1,s2,s3,s4]]) of 4, 5, 8 or 12 elements"
            std::vector<float> extraDistortionCoeffs;

            std::atomic_bool visuEnabled = false;
        };
        Properties p;

        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::FloatSpinBox markerSize;
            armarx::RemoteGui::Client::CheckBox visuEnabled;
        };
        RemoteGuiTab tab;


        CByteImage** cameraImages;
        cv::Mat cameraMatrix, distortionCoeffs;

        cv::Ptr<cv::aruco::DetectorParameters> arucoParameters = new cv::aruco::DetectorParameters();
        cv::Ptr<cv::aruco::Dictionary> arucoDictionary = new cv::aruco::Dictionary();

        std::mutex resultMutex;
        visionx::ArMarkerLocalizationResultList lastLocalizationResult;

    };

}

