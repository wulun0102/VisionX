/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::DepthFilter
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// IVT
#include <Image/ByteImage.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <VisionX/components/pointcloud_core/PointCloudAndImageProcessor.h>


namespace visionx
{
    namespace depthfilter

    {

        class Component:
            virtual public visionx::PointCloudAndImageProcessor
        {

        public:

            enum class replace_mode
            {
                exceed_threshold,
                undercut_threshold
            };

        private:

            std::string m_ipc_in;
            std::string m_ipc_out;
            unsigned int m_threshold;
            replace_mode m_mode;
            armarx::DrawColor24Bit m_color_depth;
            armarx::DrawColor24Bit m_color_invalid;
            CByteImage** m_buffer;
            CByteImage** m_buffer_single;
            CByteImage* m_buffer_mask;
            int m_grow_radius = 10;

        public:

            virtual ~Component() override;

            virtual void onInitPointCloudAndImageProcessor() override;
            virtual void onConnectPointCloudAndImageProcessor() override;
            virtual void onDisconnectPointCloudAndImageProcessor() override;
            virtual void onExitPointCloudAndImageProcessor() override;

            /**
             * @see armarx::ManagedIceObject::getDefaultName()
             */
            virtual std::string getDefaultName() const override;

            /**
             * @see visionx::PointCloudAndImageProcessor::process()
             */
            virtual void process() override;

            /**
             * @see PropertyUser::createPropertyDefinitions()
             */
            virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

            static void applyDepthFilter(
                const CByteImage& depth,
                unsigned int threshold,
                int grow_radius,
                replace_mode mode,
                const armarx::DrawColor24Bit& color_depth,
                const armarx::DrawColor24Bit& color_invalid,
                CByteImage& rgb,
                CByteImage& mask
            );

        };

    }
}


namespace visionx
{
    using DepthFilter = visionx::depthfilter::Component;
}
