/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


// IVT
#include "Image/ByteImage.h"
#include "Math/Math2d.h"
#include "Math/FloatVector.h"

// C
#include <math.h>



// probability distribution of a pixel in HS-space
struct CPixelProbabilityDistribution
{
    Vec2d vMean;            // mean of the distribution
    Mat2d mCovariance;      // inverse covariance matrix of the distribution (Sigma^-1)

    bool bShifted;          // true if H-scale has been shifted by 128
    // to avoid overflow problems
    float fSatWeight;       // weighting for the use of S-dist only vs HS-dist
};


// class for learning, representation and segmentation of a background using
// a normal distribution in HS-space
class CGaussBackground
{
public:

    CGaussBackground(const int nWidth, const int nHeight);
    ~CGaussBackground();

    void LearnBackgroundRGB(CByteImage** pRGBImages, const int nNumImages);
    void LearnBackground(CByteImage** pHSVImages, const int nNumImages);

    void GetBinaryForegroundImageRGB(const CByteImage* pInputImageRGB, CByteImage* pForegroundImage);

    void SegmentImageRGB(const CByteImage* pInputImageRGB, CByteImage* pProbabilityImage);
    void SegmentImageRGB(const CByteImage* pInputImageRGB, float* pProbabilityImage);
    void SegmentImage(const CByteImage* pInputImageRGB, const CByteImage* pInputImageHSV, CByteImage* pProbabilityImage);
    void SegmentImage(const CByteImage* pInputImageRGB, const CByteImage* pInputImageHSV, float* pProbabilityImage);

    void BinarizeAndFillHoles(CByteImage* pProbabilityImage);

private:

    inline void FillHolesHorVert(const CByteImage* pInputImage, CByteImage* pOutputImage, const int nRadius = 20);
    inline void FillHolesDiag(const CByteImage* pInputImage, CByteImage* pOutputImage, const int nRadius = 20);
    inline void FillHolesHorVertDiag(const CByteImage* pInputImage, CByteImage* pOutputImage, const int nRadius = 55);
    inline double CalcProbOfPixel(const int nIndex, const Vec2d vHS_Value);

    CPixelProbabilityDistribution* m_pPixelProbabilityDistributions;
    int m_nImageWidth, m_nImageHeight;

    CByteImage* m_pBackgroundRGB;
};


