/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ObjectLearningByPushingDefinitions.h"

#ifdef OLP_USE_LCCP

#include "LCCPSegmentation.h"


#include <ArmarXCore/core/logging/Logging.h>

// PCL
#include <pcl/segmentation/lccp_segmentation.h>
#include <pcl/segmentation/supervoxel_clustering.h>
#include <pcl/io/pcd_io.h>


//#include <cmath>



// lccpSegmentation = new LCCPSegmentationWrapper(25, 80.0, 0.0, 1.0, 4.0, false, 10.0, 0.1, 10, false, true);
// lccpSegmentation = new LCCPSegmentationWrapper(7.5, 30.0, 0.0, 1.0, 4.0, false, 10.0, 0.1, 0, false, true);
LCCPSegmentationWrapper::LCCPSegmentationWrapper(const float voxel_resolution, const float seed_resolution, const float color_importance, const float spatial_importance, const float normal_importance,
        const bool use_single_cam_transform, const float concavity_tolerance_threshold, const float smoothnessThreshold, const unsigned int min_segment_size, const bool use_extended_convexity,
        const bool use_sanity_criterion, const float maxZ)
{
    this->voxelResolution = voxel_resolution;
    this->seedResolution = seed_resolution;
    this->colorImportance = color_importance;
    this->spatialImportance = spatial_importance;
    this->normalImportance = normal_importance;
    this->useSingleCamTransform = use_single_cam_transform;
    this->concavityToleranceThreshold = concavity_tolerance_threshold;
    this->minSegmentSize = min_segment_size;
    this->useExtendedConvexity = use_extended_convexity;
    this->useSanityCriterion = use_sanity_criterion;
    this->smoothnessThreshold = smoothnessThreshold;
    this->maxZ = maxZ;
}



LCCPSegmentationWrapper::~LCCPSegmentationWrapper()
{
}


void LCCPSegmentationWrapper::CreateHypothesesFromLCCPSegments(const std::vector<CHypothesisPoint*>& inputPointCloud, const int maxNumHypotheses,
        CVec3dArray*& hypothesesFromLCCP, int& numFoundSegments)
{
    // convert points to PCL format
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr inputPointCloudPCL(new pcl::PointCloud<pcl::PointXYZRGBA>);
    inputPointCloudPCL->resize(inputPointCloud.size());

    for (size_t i = 0; i < inputPointCloud.size(); i++)
    {
        if (inputPointCloud.at(i)->vPosition.z <= maxZ)
        {
            inputPointCloudPCL->at(i) = ConvertHypothesisPointToPCL(inputPointCloud.at(i));
        }
    }

#ifdef OLP_MAKE_LCCP_SEG_IMAGES
    std::string filename = OLP_SCREENSHOT_PATH;
    filename.append("scene.pcd");
    pcl::io::savePCDFileASCII(filename, *inputPointCloudPCL);
#endif

    pcl::SupervoxelClustering<pcl::PointXYZRGBA> super(voxelResolution, seedResolution);
    super.setUseSingleCameraTransform(useSingleCamTransform);
    super.setInputCloud(inputPointCloudPCL);
    super.setColorImportance(colorImportance);
    super.setSpatialImportance(spatialImportance);
    super.setNormalImportance(normalImportance);
    std::map<uint32_t, pcl::Supervoxel<pcl::PointXYZRGBA>::Ptr> supervoxelClusters;

    ARMARX_INFO_S << "Extracting supervoxels";
    super.extract(supervoxelClusters);
    ARMARX_VERBOSE_S << "Number of Supervoxels: " << supervoxelClusters.size();

    ARMARX_VERBOSE_S << "Getting supervoxel adjacency";

    std::multimap<uint32_t, uint32_t> supervoxelAdjacency;
    super.getSupervoxelAdjacency(supervoxelAdjacency);

    /// LCCP for merging Supervoxels
    ARMARX_INFO_S << "Starting LCCP segmentation";
    pcl::LCCPSegmentation<pcl::PointXYZRGBA> lccp;
    lccp.setConcavityToleranceThreshold(concavityToleranceThreshold);
    lccp.setSanityCheck(useSanityCriterion);
    lccp.setSmoothnessCheck(true, voxelResolution, seedResolution, smoothnessThreshold);
    uint kFactor = useExtendedConvexity ? 1 : 0;
    lccp.setKFactor(kFactor);
    lccp.setMinSegmentSize(minSegmentSize);
    lccp.setInputSupervoxels(supervoxelClusters, supervoxelAdjacency);
    lccp.segment();


    ARMARX_VERBOSE_S << "Interpolation voxel cloud -> input cloud and relabeling";
    pcl::PointCloud<pcl::PointXYZL>::Ptr cloudWithSegmentationLabels = super.getLabeledCloud(); // this gets the points with their supervoxel index label
    lccp.relabelCloud(*cloudWithSegmentationLabels); // this overwrites the labels with those from the segmentation

    ARMARX_INFO_S << "Finished LCCP segmentation";

    // return segments
    for (int i = 0; i < maxNumHypotheses; i++)
    {
        hypothesesFromLCCP[i].Clear();
    }

    numFoundSegments = -1;

    for (size_t i = 0; i < cloudWithSegmentationLabels->size(); i++)
    {
        int label = (int)cloudWithSegmentationLabels->at(i).label;

        if (label < maxNumHypotheses)
        {
            pcl::PointXYZL pointPCL = cloudWithSegmentationLabels->at(i);
            Vec3d pointIVT = {pointPCL.getVector3fMap()(0), pointPCL.getVector3fMap()(1), pointPCL.getVector3fMap()(2)};
            hypothesesFromLCCP[label].AddElement(pointIVT);

            if (label > numFoundSegments)
            {
                numFoundSegments = label;
            }
        }
    }

    numFoundSegments += 1;
}



pcl::PointXYZRGBA LCCPSegmentationWrapper::ConvertHypothesisPointToPCL(const CHypothesisPoint* point)
{
    pcl::PointXYZRGBA result;
    result.getVector3fMap() = Eigen::Vector3f(point->vPosition.x, point->vPosition.y, point->vPosition.z);
    result.r = point->fColorR;
    result.g = point->fColorG;
    result.b = point->fColorB;
    return result;
}


#endif // OLP_USE_LCCP


