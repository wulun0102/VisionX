/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectHypothesis.h"
#include "ColorICP.h"

// PCL
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>


class CPointCloudRegistration
{
public:

    CPointCloudRegistration();
    ~CPointCloudRegistration();

    // set the scene point clound within which the object will be searched
    void SetScenePointcloud(const std::vector<CHypothesisPoint*>& aScenePoints);

    float EstimateTransformation(const std::vector<CHypothesisPoint*>& aObjectPoints, const Vec3d center, Mat3d& mRotation, Vec3d& vTranslation, const Vec3d upwardsVector,
                                 const int nEffortLevel = OLP_EFFORT_POINTCLOUD_MATCHING, const std::vector<Vec3d>* pPossibleLocationOffsets = NULL,
                                 std::vector<CColorICP::CPointXYZRGBI>* pNearestNeighbors = NULL, std::vector<float>* pPointMatchDistances = NULL,
                                 const std::vector<CHypothesisPoint*>* pAdditionalPoints = NULL, const float maxAcceptedDistance = FLT_MAX);

    float EstimateTransformation(const CObjectHypothesis* pHypothesis, Mat3d& mRotation, Vec3d& vTranslation, const Vec3d upwardsVector,
                                 const int nEffortLevel = OLP_EFFORT_POINTCLOUD_MATCHING, const std::vector<Vec3d>* pPossibleLocations = NULL,
                                 std::vector<CColorICP::CPointXYZRGBI>* pNearestNeighbors = NULL, std::vector<float>* pPointMatchDistances = NULL, const float maxAcceptedDistance = FLT_MAX);
    float EstimateTransformation(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr objectPointCloud, const Vec3d center, Mat3d& mRotation, Vec3d& vTranslation, const Vec3d upwardsVector,
                                 const int nEffortLevel, const std::vector<Vec3d>* pPossibleLocationOffsets = NULL,
                                 std::vector<CColorICP::CPointXYZRGBI>* pNearestNeighbors = NULL, std::vector<float>* pPointMatchDistances = NULL);

    // returns true if the hypothesis is matched well at its current position
    bool CheckObjectMatchAtOriginalPosition(const CObjectHypothesis* pHypothesis, float& distance, const int nEffortLevel = OLP_EFFORT_POINTCLOUD_MATCHING);

    static void RotatePoints(std::vector<CColorICP::CPointXYZRGBI>& points, const Mat3d rot);

    static void GetCompleteTransformation(Mat3d rotICP, Vec3d transICP, Mat3d rotShift, Vec3d transShift, Vec3d center, Mat3d& completeRotation, Vec3d& completeTranslation);

    static CColorICP::CPointXYZRGBI ConvertPclToXYZRGBI(pcl::PointXYZRGBA point);

private:
    CColorICP** m_pColorICPInstances;
};


