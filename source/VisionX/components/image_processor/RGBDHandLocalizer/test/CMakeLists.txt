
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RGBDHandLocalizer)
 
armarx_add_test(RGBDHandLocalizerTest RGBDHandLocalizerTest.cpp "${LIBS}")