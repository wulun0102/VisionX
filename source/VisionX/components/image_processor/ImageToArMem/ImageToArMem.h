/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>

// STD/STL
#include <memory>
#include <mutex>
#include <string>
#include <vector>

// IVT
#include <Image/ByteImage.h>

// Ice
#include <Ice/Current.h>
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/interface/core/ImageProviderInterface.h>

// Aron/ArMem Stuff
// #include <RobotAPI/interface/aron.h> // not necessary
#include <RobotAPI/interface/armem/WritingInterface.h>
#include <RobotAPI/libraries/armem/client/MemoryWriter.h>

// Generated
#include <VisionX/components/armem/ArMemVisionMemory/aron/ImageRGB.aron.generated.h>


namespace visionx
{
    class ImageToArMem :
        public visionx::ImageProcessor
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        // ImageProcessor interface
        void onInitImageProcessor() override;
        void onConnectImageProcessor()  override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;

        void process() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        void addProviderSegments();
        void commitImages();


    private:

        std::string imageProviderName = "ImageProvider";

        visionx::ImageProviderInterfacePrx imageProvider;
        visionx::ImageProviderInfo imageProviderInfo;
        armarx::MetaInfoSizeBase::PointerType imageMetaInfo;
        IceUtil::Time timeLastImage;

        // ImageBuffer und ImageInformations
        std::mutex inputImageMutex;
        CByteImage** inputImageBuffer;

        std::vector<armarx::armem::ImageRGB> aronImagesRGB;
        // armarx::armem::ImageDepth aronImageDepth;

        int numReceivedImages;


        struct Properties
        {
            struct Image
            {
                Image(std::string coreSegmentID, std::string providerSegmentName, std::string entityName,
                      std::vector<size_t> imageIndices) :
                    coreSegmentID(coreSegmentID), providerSegmentName(providerSegmentName), entityName(entityName),
                    imageIndices(imageIndices)
                {
                }

                std::string coreSegmentID = "";
                std::string providerSegmentName = "";
                std::string entityName = "images";

                std::vector<size_t> imageIndices;

                armarx::armem::MemoryInterfacePrx memory; // WritingInterfacePrx
                armarx::armem::MemoryID entityID;

                float fps = 10;
            };
            Image rgb { "Vision/ImageRGB", "", "images", {0}};
            Image depth { "Vision/ImageDepth", "", "images", {1}};
        };
        Properties p;

    };
}
