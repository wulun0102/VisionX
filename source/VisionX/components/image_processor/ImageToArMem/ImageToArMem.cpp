/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageToArMem.h"

// IVT
#include <Image/ImageProcessor.h>


namespace armem = armarx::armem;


namespace visionx
{

    string ImageToArMem::getDefaultName() const
    {
        return "ImageToArMem";
    }


    armarx::PropertyDefinitionsPtr ImageToArMem::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs(new armarx::ComponentPropertyDefinitions{getConfigIdentifier()});

        defs->optional(imageProviderName, "img.ProviderName", "Name of the image provier.");

        // defs->defineOptionalProperty<std::vector<std::string>>("", {}, "");
        defs->optional(p.rgb.coreSegmentID, "mem.rgb.CoreSegmentID", "ID of the RGB image core segment.");
        defs->optional(p.rgb.providerSegmentName, "mem.rgb.ProviderSegmentName", "Name of the RGB image provider segment. Leave empty to use this component's name.");
        defs->optional(p.rgb.entityName, "mem.rgb.EntityName", "Name of the RGB images entity.");
        defs->component(p.rgb.memory, "ArMemVisionMemory", "mem.rgb.MemoryName", "Name fo the RGB memory component. To be replaced by Memory Name System.");

        defs->optional(p.depth.coreSegmentID, "mem.depth.CoreSegmentID.", "ID of the depth image core segment.");
        defs->optional(p.depth.providerSegmentName, "mem.rgb.ProviderSegmentName", "Name of the RGB image provider segment. Leave empty to use this component's name.");
        defs->optional(p.depth.entityName, "mem.depth.EntityName", "Name of the depth images entity.");
        defs->component(p.depth.memory, "ArMemVisionMemory", "mem.depth.MemoryName", "Name fo the RGB memory component. To be replaced by Memory Name System.");

        return defs;
    }


    void visionx::ImageToArMem::onInitImageProcessor()
    {
        p.rgb.entityID = armem::MemoryID::fromString(p.rgb.coreSegmentID);
        p.rgb.entityID.providerSegmentName = p.rgb.providerSegmentName.empty() ? getName() : p.rgb.providerSegmentName;
        p.rgb.entityID.entityName = p.rgb.entityName;
        // Do the same for depth.

        ARMARX_VERBOSE << "Using image provider '" << imageProviderName << "'.";

        usingImageProvider(imageProviderName);
        // setFramerate(1);
    }

    void visionx::ImageToArMem::onConnectImageProcessor()
    {
        ARMARX_CHECK_NOT_NULL(p.rgb.memory);

        // Connect to image provider.
        imageProviderInfo = getImageProvider(imageProviderName);
        imageProvider = getProxy<visionx::ImageProviderInterfacePrx>(imageProviderName);

        // Init input image.
        this->numReceivedImages = imageProviderInfo.numberImages;
        ARMARX_CHECK_NONNEGATIVE(numReceivedImages);
        this->inputImageBuffer = new ::CByteImage*[size_t(numReceivedImages)];
        for (int i = 0; i < numReceivedImages; ++i)
        {
            inputImageBuffer[i] = visionx::tools::createByteImage(imageProviderInfo);
        }

        addProviderSegments();
    }

    void visionx::ImageToArMem::onDisconnectImageProcessor()
    {
        // Clear input image buffer.
        {
            CByteImage** buffer = inputImageBuffer;
            inputImageBuffer = nullptr;
            for (int i = 0; i < numReceivedImages; ++i)
            {
                delete buffer[i];
            }
            delete[] buffer;
        }
    }

    void visionx::ImageToArMem::onExitImageProcessor()
    {
    }


    void visionx::ImageToArMem::process()
    {
        const IceUtil::Time timeout = IceUtil::Time::milliSeconds(1000);

        int numImages = 0;
        if (waitForImages(imageProviderName, static_cast<int>(timeout.toMilliSeconds())) && inputImageBuffer)
        {
            numImages = getImages(imageProviderName, inputImageBuffer, imageMetaInfo);
        }
        else
        {
            ARMARX_WARNING << "Timeout while waiting for camera images (> " << timeout << ").";
            return;
        }

        ARMARX_DEBUG << "Received " << numImages << " new images. Putting image to member variable.";
        if (numImages == numReceivedImages)
        {
            for (size_t i : p.rgb.imageIndices)
            {
                while (i >= aronImagesRGB.size())
                {
                    aronImagesRGB.emplace_back();
                }

                CByteImage* inputImage = inputImageBuffer[i];
                armem::ImageRGB& aronImage = aronImagesRGB.at(i);
                if (aronImage.image.get() == nullptr)
                {
                    ARMARX_ERROR << "The aron image container contains null. This should not happen, did you manipulate the aron class?";
                }
                if (!(aronImage.image->width == inputImage->width && aronImage.image->height == inputImage->height))
                {
                    ARMARX_DEBUG << "Changing ARON image to " << inputImage->width << "x" << inputImage->height << ".";
                    aronImage.image->Set(inputImage->width, inputImage->height, inputImage->type);
                }
                ::ImageProcessor::CopyImage(inputImage, aronImage.image.get());
            }
            // Do the same for depth.


            timeLastImage = IceUtil::Time::microSeconds(imageMetaInfo->timeProvided);
            commitImages();

        }
        else
        {
            ARMARX_ERROR << "Received unexpected number of input images. Got " << numImages << " instead of " <<  numReceivedImages << ".";
        }
        ARMARX_DEBUG << "Wait for next image";
    }



    void ImageToArMem::addProviderSegments()
    {
        armarx::armem::MemoryWriter writer(p.rgb.memory);
        armem::data::AddSegmentsInput inputs;

        armem::data::AddSegmentInput& input = inputs.emplace_back();
        input.coreSegmentName = p.rgb.entityID.coreSegmentName;
        input.providerSegmentName = p.rgb.entityID.providerSegmentName;
        input.clearWhenExists = true;
        // Do the same for depth.

        ARMARX_CHECK_NOT_NULL(p.rgb.memory);
        armem::data::AddSegmentsResult results = writer.addSegments(inputs);
        if (!results.at(0).success)
        {
            ARMARX_ERROR << "Failed to add RGB provider segment " << p.rgb.entityID << ".\n" << results.at(0).errorMessage;
        }
    }


    void visionx::ImageToArMem::commitImages()
    {
        ARMARX_CHECK_NOT_NULL(p.rgb.memory);
        armem::MemoryWriter writer(p.rgb.memory);
        armem::Commit commit;

        {
            ARMARX_DEBUG << "Got a new image for processing. Send image to vision memory.";

            armem::EntityUpdate& update = commit.updates.emplace_back();
            update.entityID = p.rgb.entityID;
            update.timeCreated = timeLastImage;

            TIMING_START(ToAron);
            for (const auto& img : aronImagesRGB)
            {
                update.instancesData.push_back(img.toAron());
            }
            TIMING_END(ToAron);
        }
        TIMING_START(Commit);
        armem::CommitResult results = writer.commit(commit);
        TIMING_END(Commit);

        ARMARX_CHECK_EQUAL(results.results.size(), 1);
        armem::EntityUpdateResult& result = results.results.at(0);
        ARMARX_DEBUG << result;


        ARMARX_DEBUG << deactivateSpam(0.5) << "Processed a new image and sent it to the memory";
    }

}
