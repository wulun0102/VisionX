/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2018, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Julian Zimmer ( urdbu at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef _H_VISIONX_MaskRobotInImage_
#define _H_VISIONX_MaskRobotInImage_


#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <Eigen/Core>


#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>

#include <VisionX/interface/core/DataTypes.h>


#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/SoInteraction.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoUnits.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class MaskRobotInImage
     * @brief A brief description
     *
     * Detailed Description
     */
    class MaskRobotInImage
    {
    public:
        /**
         * MaskRobotInImage Constructor
         */
        MaskRobotInImage(RobotStateComponentInterfacePrx robotStateComponent, std::string cameraFrameName, visionx::ImageFormatInfo imageFormat, float fov, SbColor backgroundColor = SbColor(0.f, (177.f / 255.f), (64.f / 255.f)), bool flipImages = true, bool useFullModel = true, float collisionModelInflationMargin = 0.f);

        /**
         * MaskRobotInImage Destructor
         */
        ~MaskRobotInImage();

        CByteImage* getMaskImage(Ice::Long timestamp);

        void setBackgroundColor(SbColor newBackgroundColor);

    private:

        int imageWidth;
        int imageHeight;
        std::string cameraFrameName;
        float fov;

        SbColor backgroundColor;
        bool flipImages;
        bool useFullModel;
        float collisionModelInflationMargin;

        RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;

        std::shared_ptr<SoOffscreenRenderer>  renderer;
        std::vector<unsigned char> renderBuffer;

        CByteImage* renderedImage;
        CByteImage* maskImage;
    };
}
#endif
