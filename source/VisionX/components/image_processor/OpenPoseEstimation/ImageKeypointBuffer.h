/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::ImageKeypointBuffer
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "KeypointManager.h"
#include <VisionX/core/ImageProcessor.h>

#include <mutex>

namespace armarx
{
    /**
     * @class ImageKeypointBuffer
     * @brief A brief description
     *
     * Detailed Description
     */
    class ImageKeypointBuffer
    {
    public:

        struct ImageKeypointTriple
        {
            ImageKeypointTriple()
            {
                rgbImage = nullptr;
                depthImage = nullptr;
                keypoints.reset();
            }

            ~ImageKeypointTriple()
            {
                delete rgbImage;
                delete depthImage;
                keypoints.reset();
            }

            CByteImage* rgbImage;
            CByteImage* depthImage;
            KeypointManagerPtr keypoints;
        };
        using  ImageKeypointTriplePtr = std::shared_ptr<ImageKeypointTriple>;

        /**
         * ImageKeypointBuffer Constructor
         */
        ImageKeypointBuffer(const visionx::ImageProviderInfo& imageProviderInfo);

        /**
         * ImageKeypointBuffer Destructor
         */
        ~ImageKeypointBuffer();

        bool addRGBImage(CByteImage* rgbImage, long timestamp);
        bool addDepthImage(CByteImage* depthImage, long timestamp);
        bool addKeypoints(KeypointManagerPtr keypoints, long timestamp);

        ImageKeypointTriplePtr getTripleAtTimestamp(long timestamp, bool deleteOlderTriples = true);
        std::pair<long, ImageKeypointBuffer::ImageKeypointTriplePtr> getOldestTriple();
        std::pair<long, ImageKeypointBuffer::ImageKeypointTriplePtr> getOldestCompleteTriple(bool deleteOlderTriples = true);

        void clear();

    private:
        std::mutex _bufferMutex;
        std::map<long, ImageKeypointTriplePtr> _buffer;
        visionx::ImageProviderInfo _imageProviderInfo;

        ImageKeypointTriplePtr ensureTriple(long timestamp);
        bool isComplete(long timestamp);
        void deleteOlderEntries(std::map<long, ImageKeypointBuffer::ImageKeypointTriplePtr>::iterator it);
    };
    typedef std::shared_ptr<ImageKeypointBuffer> ImageKeypointBufferPtr;
}
