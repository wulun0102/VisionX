armarx_component_set_name("ValveAttention")

set(COMPONENT_LIBS    
    ArmarXCore
    RobotAPIInterfaces
    RobotAPICore
    EarlyVisionGraph
    VisionXInterfaces
    VisionXPointCloud
    VisionXCore
    VisionXTools
)

set(SOURCES ValveAttention.cpp hog.cpp choughcirclesdemo.cpp)
set(HEADERS ValveAttention.h   hog.h   choughcirclesdemo.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
