/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemoryImageConnector
 * @author     Fabian Peller ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>

// STD/STL
#include <memory>
#include <mutex>
#include <string>
#include <vector>

// IVT

// Ice
#include <Ice/Current.h>
#include <IceUtil/Time.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <MemoryX/components/SimpleEpisodicMemory/SimpleEpisodicMemoryConnector.h>
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>

namespace visionx
{

    class SimpleEpisodicMemoryOpenPoseEstimationConnector :
        virtual public armarx::Component,
        virtual public armarx::OpenPose3DListener,
        public memoryx::SimpleEpisodicMemoryConnector
    {
    public:
        std::string getDefaultName() const override
        {
            return "SimpleEpisodicMemoryOpenPoseEstimationConnector";
        }

        void onInitComponent();
        void onConnectComponent();
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void report3DKeypoints(const armarx::Keypoint3DMapList&, long, const Ice::Current&);

    protected:


    private:
    };
}
