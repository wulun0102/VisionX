/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMemVisionMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>


#include <RobotAPI/libraries/armem/memory/Memory.h>
#include <RobotAPI/libraries/armem/component/MemoryComponentPlugin.h>


namespace armarx
{
    /**
     * @class ArMemVisionMemoryPropertyDefinitions
     * @brief Property definitions of `ArMemVisionMemory`.
     */
    class ArMemVisionMemoryPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArMemVisionMemoryPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-ArMemVisionMemory ArMemVisionMemory
     * @ingroup VisionX-Components
     * A description of the component ArMemVisionMemory.
     *
     * @class ArMemVisionMemory
     * @ingroup Component-ArMemVisionMemory
     * @brief Brief description of class ArMemVisionMemory.
     *
     * Detailed description of class ArMemVisionMemory.
     */
    class ArMemVisionMemory :
        virtual public armarx::Component
      , virtual public armem::MemoryComponentPluginUser
      , virtual public LightweightRemoteGuiComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        // WritingInterface interface
    public:
        armem::data::AddSegmentsResult addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&) override;
        armem::data::CommitResult commit(const armem::data::Commit& commit, const Ice::Current&) override;


        // ReadingInterface interface
    public:
        using MemoryComponentPluginUser::getEntitySnapshots;


        // LightweightRemoteGuiComponentPluginUser interface
    public:
        void RemoteGui__createTab();
        void RemoteGui_update() override;


    private:


        struct Properties
        {
        };
        Properties p;


        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            std::atomic_bool rebuild = false;

            RemoteGui::Client::GroupBox memoryGroup;
        };
        RemoteGuiTab tab;

    };
}
