/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ArMemVisionMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMemVisionMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/armem/error/ArMemError.h>
#include <RobotAPI/libraries/armem/component/MemoryRemoteGui.h>


namespace armarx
{
    ArMemVisionMemoryPropertyDefinitions::ArMemVisionMemoryPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
    }

    armarx::PropertyDefinitionsPtr ArMemVisionMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ArMemVisionMemoryPropertyDefinitions(getConfigIdentifier());

        memory.name = "Vision";
        defs->optional(memory.name, "memory.Name", "Name of this memory.");

        return defs;
    }

    std::string ArMemVisionMemory::getDefaultName() const
    {
        return "ArMemVisionMemory";
    }


    void ArMemVisionMemory::onInitComponent()
    {
        memory.addCoreSegment("ImageRGB");
        memory.addCoreSegment("ImageDepth");
        memory.addCoreSegment("PointCloud");
    }


    void ArMemVisionMemory::onConnectComponent()
    {
        RemoteGui__createTab();
        RemoteGui_startRunningTask();
    }


    void ArMemVisionMemory::onDisconnectComponent()
    {
    }


    void ArMemVisionMemory::onExitComponent()
    {
    }


    // WRITING

    armem::data::AddSegmentsResult ArMemVisionMemory::addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&)
    {
        bool addCoreSegments = false;
        armem::data::AddSegmentsResult result = armem::MemoryComponentPluginUser::addSegments(input, addCoreSegments);
        tab.rebuild = true;
        return result;
    }


    armem::data::CommitResult ArMemVisionMemory::commit(const armem::data::Commit& commit, const Ice::Current&)
    {
        armem::data::CommitResult result = armem::MemoryComponentPluginUser::commit(commit);
        tab.rebuild = true;
        return result;
    }


    // READING

    // Inherited from Plugin



    // REMOTE GUI

    void ArMemVisionMemory::RemoteGui__createTab()
    {
        using namespace armarx::RemoteGui::Client;

        armem::MemoryRemoteGui mrg;
        {
            std::scoped_lock lock(memoryMutex);
            tab.memoryGroup = mrg.makeGroupBox(memory);
        }

        VBoxLayout root = {tab.memoryGroup, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void ArMemVisionMemory::RemoteGui_update()
    {
        if (tab.rebuild.exchange(false))
        {
            RemoteGui__createTab();
        }
    }

}
