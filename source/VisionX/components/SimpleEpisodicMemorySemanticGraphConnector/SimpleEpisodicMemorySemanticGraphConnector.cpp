/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SimpleEpisodicMemorySemanticGraphConnector
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleEpisodicMemorySemanticGraphConnector.h"


namespace visionx
{
    armarx::PropertyDefinitionsPtr
    SimpleEpisodicMemorySemanticGraphConnector::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def{new armarx::ComponentPropertyDefinitions{getConfigIdentifier()}};

        def->topic<armarx::semantic::GraphStorageTopic>("SemanticGraphTopic", "SemanticGraphTopicName");
        def->optional(graphId, "GraphId", "Id of the graph to be accepted");
        return def;
    }

    std::string SimpleEpisodicMemorySemanticGraphConnector::getDefaultName() const
    {
        return "SimpleEpisodicMemorySemanticGraphConnector";
    }

    void SimpleEpisodicMemorySemanticGraphConnector::onInitComponent()
    {
        usingProxy(m_simple_episodic_memory_proxy_name);
    }

    void SimpleEpisodicMemorySemanticGraphConnector::onConnectComponent()
    {
        getProxy(m_simple_episodic_memory, m_simple_episodic_memory_proxy_name);
    }

    void SimpleEpisodicMemorySemanticGraphConnector::onDisconnectComponent()
    {

    }

    void SimpleEpisodicMemorySemanticGraphConnector::onExitComponent()
    {

    }

    void SimpleEpisodicMemorySemanticGraphConnector::reportGraph(std::string const& id, armarx::semantic::data::Graph const& graph, const Ice::Current&)
    {
        if (id == graphId)
        {
            //semrel::AttributedGraph
            //m_simple_episodic_memory->registerGraph();
        }
    }

}
