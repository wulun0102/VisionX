/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     Kai Welke <welke at kit dot edu>
* @author     Jan Issac <jan dot issac at gmx dot de>
* @copyright  2010 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/BuiltinSequences.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/SharedMemory.ice>
#include <ArmarXCore/interface/core/PackagePath.ice>
#include <RobotAPI/interface/units/UnitInterface.ice>
#include <VisionX/interface/core/DataTypes.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{    
    interface ProviderWithSharedMemorySupportInterface
    {
        /**
         * Check if provider has shared memory support.
         */
        bool hasSharedMemorySupport();
    };

    /**
     * An image provider component captures images from cameras, files, ...
     **/
    interface ImageProviderInterface extends armarx::SensorActorUnitInterface, ProviderWithSharedMemorySupportInterface
    {
        /**
         * Retrives provider's images
         */
        armarx::Blob getImages();
        armarx::Blob getImagesAndMetaInfo(out armarx::MetaInfoSizeBase info);

        /**
         * Retrives provider's image format information via Ice.
         */
        ImageFormatInfo getImageFormat();

        /**
         * Retrives provider's image format information via Ice.
         */
        int getNumberImages();
    };


    exception FrameRateNotSupportedException extends armarx::UserException
    {
        float frameRate;
    };
    
    exception StartingCaptureFailedException extends armarx::UserException
    {
    
    };


    module imrec
    {
        /**
         * @brief Configurations for one channel of a recording (e.g., depth channel of an RGB-D rec).
         */
        struct ChannelConfig
        {
            bool disabled = false;
            string name;
            string format;
            double fps;
        };


        sequence<ChannelConfig> ChannelConfigs;


        struct ChannelPreferences
        {
            string name;
            bool requiresLossless;
        };


        sequence<ChannelPreferences> ChannelPreferencesList;


        /**
         * @brief Configurations for an entire recording (i.e., RGB-D recording, stereo recording, ...).
         */
        struct Config
        {
            string name;
            armarx::data::PackagePath location;
            long startTimestamp;
            ChannelConfigs channelConfigs;
        };


        sequence<Config> Configs;


        /**
         * @brief Configurations map to address several image providers.
         */
        dictionary<string, Config> ConfigMap;


        enum State
        {
            // No recording is running, the provider is ready to record.
            ready,
            // A recording was scheduled but is not running yet.
            scheduled,
            // A recording is running.
            running,
            // A recording is currently being stopped.
            stopping,
            // A stopped recording is currently being written to disk.  Only if buffer is not empty
            // after stopping.
            writing,
            // Image provider is offline.
            offline
        };


        sequence<State> States;


        struct Status
        {
            State type;
            long framesWritten;
            long framesBuffered;
        };


        sequence<Status> Statuses;


        dictionary<string, Status> StatusesMap;
    }


    interface RecordingImageProviderInterface extends ImageProviderInterface
    {

        /**
         * @brief Starts a recording parametrized according to given config struct.
         *
         * @param config Config struct.
         */
        bool startImageRecording(imrec::Config recConfig);

        /**
         * @brief Checks if component is recording.
         *
         * @return true if component is recording, false otherwise.
         */
        imrec::Status getImageRecordingStatus();

        /**
         * @brief Stops the recording.
         *
         * This method is blocking, i.e., it will block until the whole recording was written to
         * disk.
         */
        bool stopImageRecording();

        /**
         * @brief Return channel preferences like names or whether lossless formats are required.
         * The default implementation just returns indices for the channel names and that no
         * lossless formats are required.  This method should be overridden in image providers
         * (e.g. to return {"left", false} and {"right", false} for stereo cameras or
         * {"rgb", false}, {"depth", true} for RGB-D cameras).
         *
         * @return List of preferences corresponding to the channels.
         */
        imrec::ChannelPreferencesList getImageRecordingChannelPreferences();

    };


    enum CompressionType
    {
        eNoCompression,
        ePNG,
        eJPEG
    };

    /**
     * An image provider component captures images from cameras, files, ...
     **/
    interface CompressedImageProviderInterface extends RecordingImageProviderInterface
    {
        /**
         * Retrieves provider's images
         */
        armarx::Blob getCompressedImagesAndMetaInfo(CompressionType compressionType, int compressionQuality, out armarx::MetaInfoSizeBase info);
    };


    interface CapturingImageProviderInterface extends CompressedImageProviderInterface
    {
        /**
         * Initiate image capturing.
         *
         * @param framesPerSecond FPS that has to be ensure if the synchronization
         *        mode is set to eFpsSynchronization, otherwise this won't have any
         *        effect.
         */
        void startCapture(float framesPerSecond) throws FrameRateNotSupportedException,
                                                        StartingCaptureFailedException;
        
        /**
         * Suspends image capturing and hence image update notification
         * broadcasts
         */
        void stopCapture();
    };

};

