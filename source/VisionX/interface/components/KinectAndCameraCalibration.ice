/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Natsuki Yamanobe ( natsuki dot yamanobe at partner dot kit dot edu )
 * @copyright  2015
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <RobotAPI/interface/core/PoseBase.ice>
//#include <ArmarXCore/interface/observers/Matrix.ice>

module visionx
{    
    struct KinectPoseCalibration
    {
        bool result;
        armarx::PoseBase transformation;
    };

    interface KinectAndCameraCalibrationInterface extends ImageProcessorInterface
    {
        void startCalibration();
        void stopCalibration();
        KinectPoseCalibration getCalibrationParameters();
    };

    interface KinectAndCameraCalibrationListener extends armarx::ObserverInterface
    {
        void reportCalibrationFinished(bool calibrationFinished);
    };
};
 
