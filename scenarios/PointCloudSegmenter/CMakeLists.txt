# Add your components below as shown in the following example:
#
# set(SCENARIO_COMPONENTS
#    ConditionHandler
#    Observer
#    PickAndPlaceComponent)

set(SCENARIO_COMPONENTS
    PointCloudSegmenterApp
    )

# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("PointCloudSegmenter" "${SCENARIO_COMPONENTS}")
 
