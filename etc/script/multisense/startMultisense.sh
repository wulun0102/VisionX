#!/bin/sh

# Works for Ubuntu 14.04 and ROS indigo only
sudo stop network-manager
sudo /opt/ros/indigo/lib/multisense_bringup/configureNetwork.sh
