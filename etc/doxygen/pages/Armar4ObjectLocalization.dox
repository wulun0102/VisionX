/*!
\page VisionX-HowTos-Localizing-textured-objects-with-Armar4 Localizing textured objects with Armar4



\section VisionX-HowTos-Localizing-textured-objects-with-Armar4-Creating Creating a new scenario

In the \a ScenarioManager, create a new scenario in the Armar4 package. This scenario will receive images from the \a RealArmar4Vision scenario and will localize objects by finding their texture in the images provided.

The two components needed for the localization are the \a ObjectMemoryObserver and the \a TexturedObjectRecognition.

There are quite a lot of components that the main components depend on (directly or indirectly), which also need to be added:

\a TCPControlUnit, \a RobotStateComponent, \a KinematicUnitSimulation, \a ForceTorqueUnitSimulation, \a ForceTorqueObserver, \a WorkingMemory, \a PriorKnowledge, \a LongtermMemory and \a CommonStorage.

\section VisionX-HowTos-Localizing-textured-objects-with-Armar4-Configuring Configuring the components

While the \a ObjectMemoryObserver doesn't have any settings that need to be changed, the \a TexturedObjectRecognition does. Set the \a AgentName to \a Armar4 and the \a ImageProviderName to \a Armar4ImageProvider.

The configurations that need to be edited for the other components are the following:


\a TCPControlUnit:
\code
KinematicUnitName = KinematicUnitSimulation
\endcode

\a RobotStateComponent:
\code
AgentName = Armar4
RobotFileName = Armar4/robotmodel/Armar4.xml
RobotNodeSetName = Robot
\endcode

\a KinematicUnitSimulation:
\code
RobotFileName = Armar4/robotmodel/Armar4.xml
RobotNodeSetName = Robot
\endcode

\a ForceTorqueUnitSimulation:
\code
AgentName = Armar4
SensorNames = TCP L, TCP R
\endcode

\a ForceTorqueObserver:
\code
ForceTorqueTopicName = ForceTorqueValues
\endcode

\a WorkingMemory:
no changes

\a PriorKnowledge:
\code
ClassCollections = memdb.Prior_KitchenKK,memdb.Prior_KitchenKKObjects
GraphCollections = memdb.Prior_KitchenKKGraphs
RelationCollections = memdb.Prior_classrel
\endcode

\a LongtermMemory:
\code
DatabaseName = memdb
\endcode

\a CommonStorage:
\code
MongoAuth = no
MongoHost = 141.3.88.106
MongoPassword = 1
MongoUser = 1
\endcode

\note The \a MongoHost in the \a CommonStorage configuration is set to the IP of the Armar4 head pc, since the mongo database was started on that pc.

\image html Localizing-Objects-setup-not-running.png "As long as the new scenario isn't running, the ObjectLocalizationRequester widget isn't responsive"

The object localization depends on a robot node in the robot model of Armar4. The model has to include the nodes of the head of Armar4. Therefore, you need to navigate to \a Armar4/data/Armar4/robotmodel/ and check \a Armar4.xml. Look for the line which includes Armar4-Head.xml:

\code

<File importEEF="true">Armar4-Head.xml</File>

\endcode

If it is commented out, or the value is set to false, just replace the line with the line above.


\section VisionX-HowTos-Localizing-textured-objects-with-Armar4-Running Running the scenario

To test the scenario, you have to have the \a RealArmar4Vision scenario run on the head pc, as explained in previous how tos. Check its image output with the \a ImageMonitor in the gui. To request an object, the \a ObjectLocalizationRequester widget needs to be added to the gui as well. Start the newly created scenario from the \a ScenarioManager and the \a ObjectLocalizationRequester should become available. 

\image html Localizing-Objects-setup-running.png "The ObjectLocalizationRequester becomes available when the scenario is running. Chose the desired object from the left drop down menu"

You can choose the object you want to localize from the list on the left. Press the request button to get the localization process going. Switch over to the \a ImageMonitor and change its settings. A new proxy named \a TexturedObjectRecognitionResult should be available. Upon selecting it and confirming, you should be able to see the camera images, just as before. But when you hold the requested object into the camera view, it will be detected and outlined in red. Texture feature points will be displayed. The existence of the object can also be checked in the \a WorkingMemoryGui under the \a ObjectInstances tab.

\image html Localizing-Objects-image-provider-setup.png "The image provider can display the results of the localization"

\image html Localizing-Objects-results.png "Localization of the multivitamin juice"

\image html Localizing-Objects-in-memory.png "The localized object is written to the memory and can be found under the objectInstances tab"

\note The texture recognition is not trained for close objects or very far objects.


*/
